package com.georgewayTutor;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

import com.georgewayTutor.utils.AppUtils;
import com.georgewayTutor.utils.ViewAnimUtils;
import com.google.firebase.iid.FirebaseInstanceId;

/**
 * Created by JAI on 1/7/2018.
 */

public class SplashScreen_Activity extends AppCompatActivity {
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1001;
    private Button getStartBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);
        AppUtils.mGenerateHashKey(SplashScreen_Activity.this);

        if (Georgeway_Application.getSharedPreferences().getString("isLogin", "").equalsIgnoreCase("true")) {
            startActivity(new Intent(SplashScreen_Activity.this, MainActivity.class));
            ViewAnimUtils.activityEnterTransitions(SplashScreen_Activity.this);
            finishAffinity();
        }
        Log.e("FireBase Id", "" + Georgeway_Application.getSharedPreferences().getString(AppUtils.FCM_TOKEN,""));
        initView();
    }

    private void initView() {
        getStartBtn = (Button) findViewById(R.id.getStartBtn);
        getStartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkPermission();
            }
        });
    }

    private void callNextActivity() {
        if (Georgeway_Application.getSharedPreferences().getString("isLogin", "").equalsIgnoreCase("true")) {
            startActivity(new Intent(SplashScreen_Activity.this, MainActivity.class));
            ViewAnimUtils.activityEnterTransitions(SplashScreen_Activity.this);
            finishAffinity();
        } else {
            startActivity(new Intent(SplashScreen_Activity.this, Login_Activity.class));
            ViewAnimUtils.activityEnterTransitions(SplashScreen_Activity.this);
            finishAffinity();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.switchmenu, menu);
        return true;
    }

    private void checkPermission() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(SplashScreen_Activity.this, android.Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(SplashScreen_Activity.this,android.Manifest.permission.RECORD_AUDIO)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                ActivityCompat.requestPermissions(SplashScreen_Activity.this,
                        new String[]{android.Manifest.permission.RECORD_AUDIO, android.Manifest.permission.CAMERA, android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);

            } else {

                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(SplashScreen_Activity.this,
                        new String[]{android.Manifest.permission.RECORD_AUDIO, android.Manifest.permission.CAMERA, android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted
            callNextActivity();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    callNextActivity();

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    ActivityCompat.requestPermissions(SplashScreen_Activity.this,
                            new String[]{android.Manifest.permission.RECORD_AUDIO, android.Manifest.permission.CAMERA, android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_READ_CONTACTS);
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }
}
