package com.georgewayTutor;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class TestActivity extends Activity {


    DatabaseReference refTutors, refUsers;
    ArrayList<Item> list, duplicateList, sortedList;
    HashSet<String> emailList;
    Item item;
    int i=0,n=0,m=0;
    String result;
    TextView tvResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        tvResult = findViewById(R.id.resultv);

        list = new ArrayList<>();
        duplicateList = new ArrayList<>();
        sortedList = new ArrayList<>();
        emailList = new HashSet<>();

        refUsers  = FirebaseDatabase.getInstance().getReference().child("users");
        refTutors  = FirebaseDatabase.getInstance().getReference().child("tutors");

        refTutors.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                i++;
                Log.e("TESTING_PK", "i = "+i);
                if (dataSnapshot!=null){
                    n++;
                    Log.e("TESTING_PK", "n = "+n);
                    if (dataSnapshot.getValue() instanceof HashMap){
                        HashMap hashmap = (HashMap) dataSnapshot.getValue();
                        if (hashmap!=null){
                            JSONObject json = new JSONObject(hashmap);
                            item= new Item();
                            JSONObject details = json.optJSONObject("details");
                            if (details!=null){
                                item.id = details.optString("userid");
                                item.email = details.optString("email");
                                emailList.add(item.email);
                                list.add(item);
                                m++;
                                Log.e("TESTING_PK", "m = "+m);
                                Log.e("TESTING_PK", "id = "+item.id);
                                Log.e("TESTING_PK", "email = "+item.email+"\n\n\n");
                            }else{
                                Log.e("TESTING_PK", "Details null, i= " + i + ", for key = "+dataSnapshot.getKey()+"\n\n\n");
                            }
                        }else{
                            Log.e("TESTING_PK", "HashMap null, i= "+i+ ", for key = "+dataSnapshot.getKey()+"\n\n\n");
                        }
                    }else{
                        Log.e("TESTING_PK", "datasnapshot null, i= "+i+"\n\n\n");
                    }
                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {}

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
    }

    private void method1(){
        result = "";
        for (Item m : list){
            boolean isFound = false;

            for (Item i : sortedList){
                if (i.email.contains(m.email)){
                    isFound = true;
                    duplicateList.add(i);
                    duplicateList.add(m);
                    break;
                }
            }

            if (!isFound) sortedList.add(m);
        }

        int p=0;
        for (Item z: duplicateList){
            result += p + ".\n";
            result += "id : " + z.id + "\n";
            result += "email : " + z.email + "\n\n\n";
            p++;
        }
    }

    private void method2(){

        result = "";
        ArrayList<Item> temp = new ArrayList<>();
        for (Item i: list){
            if (i.id.isEmpty() || i.email.isEmpty()){
                temp.add(i);
            }
        }

        list.removeAll(temp);

        for (String em : emailList){


            int numberofduplicates = -1;
            ArrayList<String> ids = new ArrayList<>();

            for (Item i : list){
                if (em.equals(i.email)){
                    numberofduplicates++;
                    ids.add(i.id);
                }
            }

            if (numberofduplicates>0){
                result += "Email : " + em;
                result += "\nDuplicates In : ";
                for (String idstr : ids){
                    result += idstr+", ";
                }
                result += ";\nNumber of duplicates : " + numberofduplicates + "\n\n\n";
            }

        }
    }

    public void onSeeResultsClicked(View view) {
        method2();
        tvResult.setText(result);
    }


    class Item {
        public String id;
        public String email;

        public Item() {
        }
    }
}
