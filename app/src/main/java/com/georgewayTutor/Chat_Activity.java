package com.georgewayTutor;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.georgewayTutor.Models.s.ChatModel;
import com.georgewayTutor.adapters.Chat_Adapter;
import com.georgewayTutor.calling.BaseActivity;
import com.georgewayTutor.calling.CallScreenActivity;
import com.georgewayTutor.calling.SinchService;
import com.georgewayTutor.drawingBoard.ui.activity.Drawing_Activity;
import com.georgewayTutor.utils.AppUtils;
import com.georgewayTutor.utils.CircularImageView;
import com.georgewayTutor.utils.ViewAnimUtils;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.sinch.android.rtc.MissingPermissionException;
import com.sinch.android.rtc.SinchError;
import com.sinch.android.rtc.calling.Call;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import id.zelory.compressor.Compressor;

/**
 * Created by JAI on 1/13/2018.
 */

public class Chat_Activity extends BaseActivity implements SinchService.StartFailedListener{

    private Toolbar toolbar;
    private TextView titleTxt;
    private DatabaseReference databaseReference;
    private ListView userChatRv;
    private LinearLayout llBottom;
    private EditText msgEt;
    private ImageButton sendImageBtn;
    private ImageButton sendBtn;
    private ArrayList<JSONObject> jsonObjects;
    private Chat_Adapter chat_adapter;
    private DatabaseReference chatDatabase, userChatFirebaseDatabase, tutorChatDatabase,referenceMyself;
    private String userId = "";
    private TextView end_chat_image;
    private BottomSheetDialog bottomSheetDialog, chooseMediaMenu;
    private TextView deleteButton;
    private TextView editButton;
    private TextView cancelButton;
    private ImageView videoCallbtn, audioCallbtn;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.RECORD_AUDIO
    };
    private File filePathImageCamera = null;
    private String chatKey = "";
    private String TAG = "Chat_Actiivyt";
    private LinearLayout cameraLayout;
    private LinearLayout dawring_layout;
    private LinearLayout galleryLayout;
    private TextView cancelTxt;
    private String type = "";
    private String userImage = "";
    private ProgressDialog progressDialog;
    private ImageView drawingBtn;
    private CircularImageView userProfileimage;
    private String capturedImageFilePath="";

    @Override
    protected void onServiceConnected() {
        getSinchServiceInterface().setStartListener(this);
        if (!getSinchServiceInterface().isStarted()){
            getSinchServiceInterface().startClient("T_" + Georgeway_Application.getSharedPreferences().getString("userId",""));
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_activity);

        startService(new Intent(this,SinchService.class));


        progressDialog = AppUtils.getProgressDialog(Chat_Activity.this);
        initView();
        if (getIntent() != null) {
            if (getIntent().getStringExtra("chatkey") != null)
            {
                chatKey = getIntent().getStringExtra("chatkey");
                jsonObjects = new ArrayList<>();
                userImage = getIntent().getStringExtra("image");
                if (userImage.startsWith("https:"))
                {
                    ImageLoader.getInstance().displayImage(userImage,userProfileimage);
                }else
                {
                    userImage = "https://georgewayglobal.com/"+userImage;
                    ImageLoader.getInstance().displayImage(userImage,userProfileimage);
                }

                String nameOfOtherParty = getIntent().getStringExtra("name");
                titleTxt.setText(nameOfOtherParty);
                SinchService.callerDisplayName = nameOfOtherParty;
                userId = getIntent().getStringExtra("userId");
                String myId = Georgeway_Application.getSharedPreferences().getString("userId", "");
                referenceMyself =  FirebaseDatabase.getInstance().getReference().child("tutors").child(myId);
                databaseReference = FirebaseDatabase.getInstance().getReference("chats").child(getIntent().getStringExtra("chatkey"));
                chatDatabase = FirebaseDatabase.getInstance().getReference("chats/" + getIntent().getStringExtra("chatkey"));
                tutorChatDatabase = FirebaseDatabase.getInstance().getReference("tutors/" + myId + "/chat_rooms").child(getIntent().getStringExtra("chatkey"));
                userChatFirebaseDatabase = FirebaseDatabase.getInstance().getReference("users/" + userId + "/chat_rooms").child(getIntent().getStringExtra("chatkey"));

                if (getIntent().getStringExtra("session") != null) {
                    if (getIntent().getStringExtra("session").equalsIgnoreCase("true")) {
                        llBottom.setVisibility(View.VISIBLE);
                    } else {
                        llBottom.setVisibility(View.GONE);
                    }
                }

            }else {

            }
        }

        userChatFirebaseDatabase.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot!=null) {
                    if (dataSnapshot.getKey().equalsIgnoreCase("is_active")) {
                        String value = "" + dataSnapshot.getValue();
                        if (value.equalsIgnoreCase("true")) {
                            llBottom.setVisibility(View.VISIBLE);
                        } else {
                            MainActivity.currentChat = false;
                            finish();
                            ViewAnimUtils.activityExitTransitions(Chat_Activity.this);
                        }
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot!=null) {
                    if (dataSnapshot.getKey().equalsIgnoreCase("is_active")) {
                        String value = "" + dataSnapshot.getValue();
                        if (value.equalsIgnoreCase("false")) {
                            MainActivity.currentChat = false;
                            finish();
                            ViewAnimUtils.activityExitTransitions(Chat_Activity.this);
                        }
                    }
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {}

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {}

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });


    }

    private void callVideo() {
        try {
            Call call = getSinchServiceInterface().callUserVideo("U_" + userId);
            if (call == null) {
                // Service failed for some reason, show a Toast and abort
                Toast.makeText(Chat_Activity.this, R.string.error_sinch_not_started, Toast.LENGTH_LONG).show();
                return;
            }
            String callId = call.getCallId();
            Intent callScreen = new Intent(Chat_Activity.this, CallScreenActivity.class);
            callScreen.putExtra(SinchService.CALL_ID, callId);
            callScreen.putExtra("name", titleTxt.getText().toString());
            callScreen.putExtra("image", userImage);
            startActivity(callScreen);
        } catch (MissingPermissionException e) {
            ActivityCompat.requestPermissions(Chat_Activity.this, new String[]{e.getRequiredPermission()}, 0);
        }
    }

    private void callAudio() {
        try {
            Call call = getSinchServiceInterface().callUser("T_" + userId);
            if (call == null) {
                // Service failed for some reason, show a Toast and abort
                Toast.makeText(Chat_Activity.this, R.string.error_sinch_not_started, Toast.LENGTH_LONG).show();
                return;
            }
            String callId = call.getCallId();
            Intent callScreen = new Intent(Chat_Activity.this, CallScreenActivity.class);
            callScreen.putExtra(SinchService.CALL_ID, callId);
            callScreen.putExtra("name", titleTxt.getText().toString());
            callScreen.putExtra("image", userImage);
            startActivity(callScreen);
        } catch (MissingPermissionException e) {
            ActivityCompat.requestPermissions(Chat_Activity.this, new String[]{e.getRequiredPermission()}, 0);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        jsonObjects.clear();
        if (databaseReference != null) {
            databaseReference.addChildEventListener(childEventListener);
            chat_adapter = new Chat_Adapter(Chat_Activity.this, jsonObjects, userChatRv);
            userChatRv.setAdapter(chat_adapter);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        databaseReference.removeEventListener(childEventListener);
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        titleTxt = (TextView) findViewById(R.id.title_txt);
        userChatRv = (ListView) findViewById(R.id.user_chat_rv);
        llBottom = (LinearLayout) findViewById(R.id.ll_bottom);
        msgEt = (EditText) findViewById(R.id.msg_et);
        sendImageBtn = (ImageButton) findViewById(R.id.send_image_btn);
        sendBtn = (ImageButton) findViewById(R.id.send_btn);
        end_chat_image = (TextView) findViewById(R.id.end_chat_image);
        bottomSheetDialog = new BottomSheetDialog(Chat_Activity.this);
        bottomSheetDialog.setContentView(R.layout.chat_more_options);
        deleteButton = (TextView) bottomSheetDialog.findViewById(R.id.deleteButton);
        editButton = (TextView) bottomSheetDialog.findViewById(R.id.editButton);
        cancelButton = (TextView) bottomSheetDialog.findViewById(R.id.cancelButton);
        videoCallbtn = (ImageView) findViewById(R.id.videoCallbtn);
        audioCallbtn = (ImageView) findViewById(R.id.audioCallbtn);
        userProfileimage = (CircularImageView)findViewById(R.id.userProfileimage);
        chooseMediaMenu = new BottomSheetDialog(Chat_Activity.this);
        chooseMediaMenu.setContentView(R.layout.media_choose_layout);
        cameraLayout = (LinearLayout) chooseMediaMenu.findViewById(R.id.camera_layout);
        galleryLayout = (LinearLayout) chooseMediaMenu.findViewById(R.id.gallery_layout);
        cancelTxt = (TextView) chooseMediaMenu.findViewById(R.id.cancel_txt);
        dawring_layout = (LinearLayout) chooseMediaMenu.findViewById(R.id.dawring_layout);
        drawingBtn = (ImageView)findViewById(R.id.drawingBtn);

        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String prefix = "";
                if (Chat_Adapter.selectedMessagePositionForReply>-1){
                    prefix = Chat_Adapter.replyPrefix;
                }
                if (!msgEt.getText().toString().isEmpty()) {
                    ChatModel chatModel = new ChatModel(prefix+msgEt.getText().toString(), Georgeway_Application.getSharedPreferences().getString("userId", ""), ServerValue.TIMESTAMP, "T", "", "T");
                    chatDatabase.push().setValue(chatModel);
                    tutorChatDatabase.child("last_msg").setValue(chatModel);
                    tutorChatDatabase.child("timestamp").setValue(ServerValue.TIMESTAMP);
                    tutorChatDatabase.child("is_read").setValue("true");
                    userChatFirebaseDatabase.child("last_msg").setValue(chatModel);
                    userChatFirebaseDatabase.child("timestamp").setValue(ServerValue.TIMESTAMP);
                    userChatFirebaseDatabase.child("is_read").setValue("false");
                    msgEt.setText("");
                    chat_adapter.markUnSelected();
                } else {
                    AppUtils.mShowDialog(Chat_Activity.this, "Please enter message");
                }
            }
        });

        drawingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                photoDrawringIntent();
            }
        });


        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });


        end_chat_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Chat_Activity.this);
                builder.setIcon(R.drawable.ic_launcher);

                builder.setTitle(getResources().getString(R.string.app_name));
                builder.setMessage("Are you sure, you want to close this session?");


                builder.setPositiveButton("Exit Session", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        MainActivity.currentChat = false;
                        tutorChatDatabase.setValue(null);
                        userChatFirebaseDatabase.child("is_active").setValue("false");
                        referenceMyself.child("total_request_count").setValue(0);
                        llBottom.setVisibility(View.GONE);
                        finish();
                        ViewAnimUtils.activityExitTransitions(Chat_Activity.this);
                    }
                });


                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

                AlertDialog alertDialog = builder.create();
                int textColorId = getResources().getIdentifier("alertMessage", "id", "android");
                TextView textColor = (TextView) alertDialog.findViewById(textColorId);
                if (textColor != null) {
                    textColor.setTextColor(Color.BLACK);
                }
                alertDialog.show();
            }
        });


        videoCallbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                type = "video";
                if (shouldAskPermissions()) {
                    verifyStoragePermissions();
                } else {
                    callVideo();
                }


            }
        });


        audioCallbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                type = "audio";
                if (shouldAskPermissions()) {
                    verifyStoragePermissions();
                } else {
                    callAudio();
                }

            }
        });


        sendImageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                type="camera";
                if (shouldAskPermissions()) {
                    verifyStoragePermissions();
                } else {
                    if (chooseMediaMenu.isShowing()) {
                        chooseMediaMenu.dismiss();
                    } else {
                        chooseMediaMenu.show();
                    }
                }
            }
        });
        cameraLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                type = "camera";
                //photoCameraIntent();
                launchCamera();
                chooseMediaMenu.dismiss();
            }
        });
        galleryLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                type = "gallery";
                photoGalleryIntent();
                chooseMediaMenu.dismiss();
            }
        });

        dawring_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                type = "drawring";
                photoDrawringIntent();
                chooseMediaMenu.dismiss();
            }
        });

        cancelTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseMediaMenu.dismiss();
            }
        });

        userProfileimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Chat_Activity.this, FullImage_Activity.class).putExtra("path",userImage));
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ViewAnimUtils.activityExitTransitions(Chat_Activity.this);
        finish();
    }

    private ChildEventListener childEventListener = new ChildEventListener() {
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String s) {

            if (dataSnapshot!=null  && dataSnapshot.getValue() instanceof HashMap) {
                HashMap hashMap = (HashMap) dataSnapshot.getValue();
                //jsonObjects.add(new JSONObject(hashMap));
                chat_adapter.updateChatList(new JSONObject(hashMap));
                userChatRv.smoothScrollToPosition(jsonObjects.size() - 1);
            }
        }

        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

        }

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {

        }

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    protected boolean shouldAskPermissions() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }
    /**
     * Checks if the app has permission to write to device storage
     * <p>
     * If the app does not has permission then the user will be prompted to grant permissions
     */
    public void verifyStoragePermissions() {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(Chat_Activity.this, Manifest.permission.CAMERA);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(Chat_Activity.this, PERMISSIONS_STORAGE, REQUEST_EXTERNAL_STORAGE
            );
        } else {
            if (type.equalsIgnoreCase("video"))
            {
                callVideo();
            }
            if (type.equalsIgnoreCase("audio"))
            {
                callAudio();
            }
            if (type.equalsIgnoreCase("camera"))
            {
                chooseMediaMenu.show();
            }
            if (type.equalsIgnoreCase("gallery"))
            {
                chooseMediaMenu.show();
            }

            if (type.equalsIgnoreCase("drawring"))
            {
                chooseMediaMenu.show();
            }
            // we already have permission, lets go ahead and call camera intent

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case REQUEST_EXTERNAL_STORAGE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted
                    if (type.equalsIgnoreCase("video"))
                    {
                        callVideo();
                    }
                    if (type.equalsIgnoreCase("audio"))
                    {
                        callAudio();
                    }
                    if (type.equalsIgnoreCase("camera"))
                    {
                        photoCameraIntent();
                    }
                    if (type.equalsIgnoreCase("gallery"))
                    {
                        photoGalleryIntent();
                    }

                    if (type.equalsIgnoreCase("drawring"))
                    {
                        photoDrawringIntent();
                    }
                }
                break;
        }
    }

    private static final int IMAGE_GALLERY_REQUEST = 1;
    private static final int IMAGE_CAMERA_REQUEST = 2;

    /**
     * Enviar foto tirada pela camera
     */
    private void photoCameraIntent() {
        String nomeFoto = DateFormat.format("yyyy-MM-dd_hhmmss", new Date()).toString();
        filePathImageCamera = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), nomeFoto + "camera.jpg");
        Intent it = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Uri photoURI = FileProvider.getUriForFile(Chat_Activity.this,
                BuildConfig.APPLICATION_ID + ".provider", filePathImageCamera);
        it.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
        startActivityForResult(it, IMAGE_CAMERA_REQUEST);
    }

    /**
     * Enviar foto pela galeria
     */
    private void photoGalleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Choose option"), IMAGE_GALLERY_REQUEST);
    }
    private void photoDrawringIntent() {
        Intent intent = new Intent(Chat_Activity.this,Drawing_Activity.class);
        startActivityForResult(intent, 5001);
    }
    public static final String URL_STORAGE_REFERENCE = "gs://gw-tutor.appspot.com";
    public static final String FOLDER_STORAGE_IMG = "images";
    FirebaseStorage storage = FirebaseStorage.getInstance();

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        StorageReference storageRef = storage.getReferenceFromUrl(URL_STORAGE_REFERENCE).child(chatKey).child(FOLDER_STORAGE_IMG);

        if (requestCode == IMAGE_GALLERY_REQUEST) {
            if (resultCode == RESULT_OK) {
                Uri selectedImageUri = data.getData();
                if (selectedImageUri != null) {
                    sendFileFirebase(storageRef, selectedImageUri);
                } else {
                    //URI IS NULL
                }
            }
        } else if (requestCode == IMAGE_CAMERA_REQUEST) {
            if (resultCode == RESULT_OK) {
                filePathImageCamera = compress(this,capturedImageFilePath);
                StorageReference imageCameraRef = storageRef.child(filePathImageCamera.getName() /*+ "_camera"*/);
                sendFileFirebase(imageCameraRef, filePathImageCamera);
            }
        }else if (requestCode == 5001) {
                if (data!=null)
                {
                    Log.e("Chat "," "+data.getStringExtra("path"));
                    File file = new File(data.getStringExtra("path"));
                    sendFileDrwaFirebase(storageRef,file);
                }
        }
    }

    public static File compress(Context context, String inputFilePath){
        File capturedFile = new File(inputFilePath);

        File CompressedFile=null;
        try {
            CompressedFile = new Compressor(context).compressToFile(capturedFile);
        } catch (IOException e){e.printStackTrace();}
        catch (Exception e){e.printStackTrace();}

        return CompressedFile;
    }

    public void launchCamera(){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if(takePictureIntent.resolveActivity(getPackageManager()) != null){

            File photoObj = null;
            try {
                photoObj = createImageFile(this);
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            if (photoObj != null) {
                Uri photoURI = FileProvider.getUriForFile(this, getApplicationContext().getPackageName()+".provider",photoObj);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);

                if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP) {
                    takePictureIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION|Intent.FLAG_GRANT_READ_URI_PERMISSION);

                } else {
                    List<ResolveInfo> resInfoList=
                            getPackageManager()
                                    .queryIntentActivities(takePictureIntent, PackageManager.MATCH_DEFAULT_ONLY);

                    for (ResolveInfo resolveInfo : resInfoList) {
                        String packageName = resolveInfo.activityInfo.packageName;
                        grantUriPermission(packageName, photoURI,
                                Intent.FLAG_GRANT_WRITE_URI_PERMISSION|Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    }
                }

                startActivityForResult(takePictureIntent, IMAGE_CAMERA_REQUEST);
            }
        }
    }

    public File createImageFile(Context context) throws IOException {
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        capturedImageFilePath = image.getAbsolutePath();
        return image;
    }

    /**
     * Envia o arvquivo para o firebase
     */
    private void sendFileFirebase(StorageReference storageReference, final Uri file) {
        if (storageReference != null) {
            progressDialog.show();
            final String name = DateFormat.format("yyyy-MM-dd_hhmmss", new Date()).toString();
            final StorageReference imageGalleryRef = storageReference.child(name + "_gallery");
            UploadTask uploadTask = imageGalleryRef.putFile(file);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e(TAG, "onFailure sendFileFirebase " + e.getMessage());
                    progressDialog.dismiss();
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Log.i(TAG, "onSuccess sendFileFirebase");
                    imageGalleryRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(@NonNull Uri uri) {
                            sendMediaFile(uri.toString());
                            progressDialog.dismiss();
                        }
                    });
                }
            });
        }

    }
    /**
     * Envia o arvquivo para o firebase
     */
    private void sendFileDrwaFirebase(StorageReference storageReference, final File file) {
        if (storageReference != null) {
            progressDialog.show();
            try {
                InputStream stream = new FileInputStream(file);
                final String name = DateFormat.format("yyyy-MM-dd_hhmmss", new Date()).toString();
                final StorageReference imageGalleryRef = storageReference.child(name + "_gallery");
                UploadTask uploadTask = imageGalleryRef.putStream(stream);
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e(TAG, "onFailure sendFileFirebase " + e.getMessage());
                        progressDialog.dismiss();
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        Log.i(TAG, "onSuccess sendFileFirebase");
                        imageGalleryRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(@NonNull Uri uri) {
                                sendMediaFile(uri.toString());
                                progressDialog.dismiss();
                            }
                        });
                    }
                });
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        } else {
            //IS NULL
        }

    }

    /**
     * Envia o arvquivo para o firebase
     */
    private void sendFileFirebase(final StorageReference storageReference, final File file) {
        if (storageReference != null)
        {
            progressDialog.show();
            Uri photoURI = Uri.fromFile(file);
            final UploadTask uploadTask = storageReference.putFile(photoURI);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e(TAG, "onFailure sendFileFirebase " + e.getMessage());
                    progressDialog.dismiss();
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Log.i(TAG, "onSuccess sendFileFirebase");
                    storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(@NonNull Uri uri) {
                            sendMediaFile(uri.toString());
                            progressDialog.dismiss();
                        }
                    });
                }
            });
        }
    }

    private void sendMediaFile(String image_path) {
        ChatModel chatModel = new ChatModel("Image", Georgeway_Application.getSharedPreferences().getString("userId", ""), ServerValue.TIMESTAMP, "M", image_path, "T");
        chatDatabase.push().setValue(chatModel);
        tutorChatDatabase.child("last_msg").setValue(chatModel);
        tutorChatDatabase.child("timestamp").setValue(ServerValue.TIMESTAMP);
        tutorChatDatabase.child("is_read").setValue("true");
        userChatFirebaseDatabase.child("last_msg").setValue(chatModel);
        userChatFirebaseDatabase.child("timestamp").setValue(ServerValue.TIMESTAMP);
        userChatFirebaseDatabase.child("is_read").setValue("false");
    }

    @Override
    public void onStartFailed(SinchError error) {

    }

    @Override
    public void onStarted() {

    }

}
