package com.georgewayTutor.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.georgewayTutor.Chat_Activity;
import com.georgewayTutor.Georgeway_Application;
import com.georgewayTutor.R;
import com.georgewayTutor.api.APIService;
import com.georgewayTutor.utils.AppUtils;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by JAI on 1/7/2018.
 */

public class Home_Adapter extends BaseAdapter {
    private AppCompatActivity appCompatActivity;
    private LayoutInflater layoutInflater;
    private ArrayList<JSONObject> jsonObjects;
    private ImageView userImageView;
    private TextView userMsgTxt;
    private TextView userMsgTimeTxt,userName_txt;
    private ImageView newmsg;
    private DatabaseReference databaseReference;
    public Home_Adapter(AppCompatActivity appCompatActivity, ArrayList<JSONObject> jsonObjects) {
        this.jsonObjects = jsonObjects;
        this.appCompatActivity = appCompatActivity;
        layoutInflater = (LayoutInflater) appCompatActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        databaseReference = FirebaseDatabase.getInstance().getReference("tutors/"+Georgeway_Application.getSharedPreferences().getString("userId","")).child("chat_rooms");
    }

    @Override
    public int getCount() {
        return jsonObjects.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {

        view = layoutInflater.inflate(R.layout.home_item_view, null);

        initView(view);

        try {
            if (jsonObjects.get(i).optString("is_read").equalsIgnoreCase("true"))
            {
                newmsg.setVisibility(View.GONE);
            }else
            {
                newmsg.setVisibility(View.VISIBLE);
            }
            userName_txt.setText(jsonObjects.get(i).optString("first_name")+" "+jsonObjects.get(i).optString("last_name"));
            userMsgTxt.setText(jsonObjects.get(i).optJSONObject("last_msg").optString("msg"));
            userMsgTimeTxt.setText(AppUtils.dateTime(jsonObjects.get(i).optJSONObject("last_msg").optLong("timestamp")));
            if (jsonObjects.get(i).getString("image").startsWith("http"))
            {
                ImageLoader.getInstance().displayImage( jsonObjects.get(i).getString("image"),userImageView, Georgeway_Application.intitOptions());
            }else
            {
                ImageLoader.getInstance().displayImage( APIService.API_BASE_URL+jsonObjects.get(i).getString("image"),userImageView, Georgeway_Application.intitOptions());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    databaseReference.child(jsonObjects.get(i).getString("key")).child("is_read").setValue(""+true);
                    appCompatActivity.startActivity(new Intent(appCompatActivity, Chat_Activity.class).putExtra("name",userName_txt.getText().toString())
                            .putExtra("userId",jsonObjects.get(i).getString("user_id")).putExtra("session",jsonObjects.get(i).getString("is_active"))
                            .putExtra("image", jsonObjects.get(i).getString("image")).putExtra("chatkey",jsonObjects.get(i).getString("key")));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        return view;
    }

    public void updateList(JSONObject object) {
        jsonObjects.add(0,object);
        notifyDataSetChanged();

    }

    private void initView(View view) {
        userImageView = (ImageView) view.findViewById(R.id.userImageView);
        userMsgTxt = (TextView) view.findViewById(R.id.userMsg_Txt);
        userMsgTimeTxt = (TextView) view.findViewById(R.id.userMsg_time_txt);
        userName_txt = (TextView)view.findViewById(R.id.userName_txt);
        newmsg = (ImageView)view.findViewById(R.id.newmsg);
    }
}
