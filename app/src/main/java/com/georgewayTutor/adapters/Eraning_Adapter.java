package com.georgewayTutor.adapters;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.georgewayTutor.Models.s.history.DataItem;
import com.georgewayTutor.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by JAI on 1/7/2018.
 */

public class Eraning_Adapter extends BaseAdapter {
    private AppCompatActivity appCompatActivity;
    private LayoutInflater layoutInflater;
    private TextView transactioAmountTxt;
    private TextView monthNameTxt;
    private TextView transactioidTxt;
    private TextView workTxt;
    private TextView workDurationTxt;
    private TextView dateTxt;
    private TextView transactionDateTxt;
    private List<DataItem> data;
    public Eraning_Adapter(AppCompatActivity appCompatActivity, List<DataItem> data) {
        this.appCompatActivity = appCompatActivity;
        layoutInflater = (LayoutInflater) appCompatActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        view = layoutInflater.inflate(R.layout.earning_item_view, null);
        initView(view);

        transactioAmountTxt.setText("$ "+data.get(i).getAmount());
        transactioidTxt.setText(data.get(i).getTaxId());
        transactionDateTxt.setText(mDate(data.get(i).getCreated()));
        conTime(data.get(i).getTotalTime(),workDurationTxt);
        return view;
    }

    private void initView(View view) {
        transactioAmountTxt = (TextView) view.findViewById(R.id.transactioAmount_txt);
        transactioidTxt = (TextView) view.findViewById(R.id.transactioid_txt);
        workDurationTxt = (TextView) view.findViewById(R.id.workDurationTxt);
        transactionDateTxt = (TextView) view.findViewById(R.id.transactionDateTxt);
    }

    private String mDate (String date)
    {
        String conDate = null;
        try {
            conDate = "";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("MMM dd");
            conDate = simpleDateFormat1.format(simpleDateFormat.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return conDate;
    }


    private void conTime(String time1,TextView textView)
    {
        final long diff =Long.parseLong(time1)*1000;
        long time = diff;
        long seconds = time / 1000 % 60;
        long minutes = (time / (1000 * 60)) % 60;
        long diffHours = time / (60 * 60 * 1000);

        textView.setText(twoDigitString(diffHours)+"h:"+twoDigitString(minutes)  + "m:"+ twoDigitString(seconds)+"s");

    }
    private String twoDigitString(long number) {
        if (number == 0) {
            return "00";
        } else if (number / 10 == 0) {
            return "0" + number;
        }
        return String.valueOf(number);
    }
}
