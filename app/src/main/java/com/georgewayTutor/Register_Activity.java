package com.georgewayTutor;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.georgewayTutor.api.APIService;
import com.georgewayTutor.api.GeorgewayApi;
import com.georgewayTutor.utils.AppUtils;
import com.georgewayTutor.utils.ViewAnimUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by JAI on 1/13/2018.
 */

public class Register_Activity extends AppCompatActivity {
    private Toolbar toolbar;
    private TextView titleTxt;
    private View view;
    private EditText userfirstEdittext;
    private EditText userlastEdittext;
    private EditText userEmailEdittext;
    private EditText userschoolEdittext;
    private EditText usercourseEdittext;
    private EditText userphoneEdittext;
    private EditText userpasswordEdittext;
    private EditText userageEdittext;
    private EditText userconpasswordEdittext;
    private TextView georgewayTxt;
    private Button loginBtn;
    private GeorgewayApi apiService;
    private ArrayList<String> schoolNameList, schoolIdList, courseNameList, courseIdList, selectCourse;
    private ProgressDialog progressDialog;
    private RadioGroup segmentedControl;
    private RadioButton opt1;
    private RadioButton opt2;
    private String gender = "", social_id = "", social_type = "";
    private ArrayList<String> course_ids = new ArrayList<String>();
    private String schoolIds = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_layout);
        apiService = APIService.createService(GeorgewayApi.class);
        progressDialog = AppUtils.getProgressDialog(Register_Activity.this);
        initView();

        getSchoolList();

        userschoolEdittext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final CharSequence[] dialogList = schoolNameList.toArray(new CharSequence[schoolNameList.size()]);
                AlertDialog.Builder builder = new AlertDialog.Builder(Register_Activity.this);
                builder.setTitle("Select school where you can teach")
                        .setItems(dialogList, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int item) {
                                Log.i("Dialogos", "Opción elegida: " + dialogList[item]);
                                userschoolEdittext.setText("" + dialogList[item]);
                                schoolIds = schoolIdList.get(item);
                            }
                        });

                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });

        usercourseEdittext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectCourse = new ArrayList<>();
                boolean[] is_checked = new boolean[courseNameList.size()];
                final CharSequence[] dialogList = courseNameList.toArray(new CharSequence[courseNameList.size()]);
                AlertDialog.Builder builder = new AlertDialog.Builder(Register_Activity.this);
                builder.setTitle("Select course you can teach");
                builder.setMultiChoiceItems(dialogList, is_checked,
                        new DialogInterface.OnMultiChoiceClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton, boolean isChecked) {
                                Log.e("course", " " + dialogList[whichButton]);

                                if (isChecked) {
                                    selectCourse.add("" + dialogList[whichButton]);
                                    course_ids.add(courseIdList.get(whichButton));
                                } else {
                                    selectCourse.remove("" + dialogList[whichButton]);
                                    course_ids.remove(whichButton);
                                }
                            }
                        });

                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        String name = TextUtils.join(",", selectCourse);
                        usercourseEdittext.setText(name);
                        Log.e("Register", " " + course_ids);

                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });

        if (getIntent() != null) {
            if (getIntent().getStringExtra("id") != null) {
                userfirstEdittext.setText(getIntent().getStringExtra("firstName"));
                userlastEdittext.setText(getIntent().getStringExtra("lastName"));
                userEmailEdittext.setText(getIntent().getStringExtra("email"));
                social_id = getIntent().getStringExtra("id");
                social_type = getIntent().getStringExtra("type");
                userpasswordEdittext.setVisibility(View.GONE);
                userconpasswordEdittext.setVisibility(View.GONE);
            }
        }

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (social_type.isEmpty()) {
                    if (!userfirstEdittext.getText().toString().isEmpty()) {
                        if (!userlastEdittext.getText().toString().isEmpty()) {
                            if (!userEmailEdittext.getText().toString().isEmpty()) {
                                if (AppUtils.isValidEmailAddress(userEmailEdittext, Register_Activity.this)) {
//                                    if (!userageEdittext.getText().toString().isEmpty()) {
                                        if (!userschoolEdittext.getText().toString().isEmpty()) {
                                            if (!usercourseEdittext.getText().toString().isEmpty()) {
                                                if (!userpasswordEdittext.getText().toString().isEmpty()) {
                                                    if (!userconpasswordEdittext.getText().toString().isEmpty()) {
                                                        if (userconpasswordEdittext.getText().toString().equals(userconpasswordEdittext.getText().toString())) {
                                                            mUserRegister();
                                                        } else {
                                                            AppUtils.mShowDialog(Register_Activity.this, "Password doest match.");
                                                        }
                                                    } else {
                                                        AppUtils.mShowDialog(Register_Activity.this, "Please select your confirm password.");
                                                    }
                                                } else {
                                                    AppUtils.mShowDialog(Register_Activity.this, "Please select your password.");
                                                }
                                            } else {
                                                AppUtils.mShowDialog(Register_Activity.this, "Please select your course name you can teach.");
                                            }
                                        } else {
                                            AppUtils.mShowDialog(Register_Activity.this, "Please select your school name where you can teach.");
                                        }
//                                    } else {
//                                        AppUtils.mShowDialog(Register_Activity.this, "Please enter your age.");
//                                    }
                                } else {
                                    AppUtils.mShowDialog(Register_Activity.this, "Please enter valid email id.");
                                }
                            } else {
                                AppUtils.mShowDialog(Register_Activity.this, "Please enter your email id.");
                            }
                        } else {
                            AppUtils.mShowDialog(Register_Activity.this, "Please enter your last name.");
                        }
                    } else {
                        AppUtils.mShowDialog(Register_Activity.this, "Please enter your first name.");
                    }
                } else {
                    if (!userfirstEdittext.getText().toString().isEmpty()) {
                        if (!userlastEdittext.getText().toString().isEmpty()) {
                            if (!userEmailEdittext.getText().toString().isEmpty()) {
                                if (AppUtils.isValidEmailAddress(userEmailEdittext, Register_Activity.this)) {
                                    if (!userschoolEdittext.getText().toString().isEmpty()) {
                                        if (!usercourseEdittext.getText().toString().isEmpty()) {
                                            mUserSocialRegister();
                                        } else {
                                            AppUtils.mShowDialog(Register_Activity.this, "Please select your course name you can teach.");
                                        }
                                    } else {
                                        AppUtils.mShowDialog(Register_Activity.this, "Please select your school name where you can teach.");
                                    }

                                } else {
                                    AppUtils.mShowDialog(Register_Activity.this, "Please enter valid email id.");
                                }
                            } else {
                                AppUtils.mShowDialog(Register_Activity.this, "Please enter your email id.");
                            }
                        } else {
                            AppUtils.mShowDialog(Register_Activity.this, "Please enter your last name.");
                        }
                    } else {
                        AppUtils.mShowDialog(Register_Activity.this, "Please enter your first name.");
                    }

                }
        }
    });

}

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        titleTxt = (TextView) findViewById(R.id.title_txt);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        view = (View) findViewById(R.id.view);
        userfirstEdittext = (EditText) findViewById(R.id.userfirst_edittext);
        userlastEdittext = (EditText) findViewById(R.id.userlast_edittext);
        userEmailEdittext = (EditText) findViewById(R.id.userEmail_edittext);
        userschoolEdittext = (EditText) findViewById(R.id.userschool_edittext);
        usercourseEdittext = (EditText) findViewById(R.id.usercourse_edittext);
        userphoneEdittext = (EditText) findViewById(R.id.userphone_edittext);
        userpasswordEdittext = (EditText) findViewById(R.id.userpassword_edittext);
        userageEdittext = (EditText) findViewById(R.id.userage_edittext);
        userconpasswordEdittext = (EditText) findViewById(R.id.userconpassword_edittext);
        georgewayTxt = (TextView) findViewById(R.id.georgewayTxt);
        loginBtn = (Button) findViewById(R.id.loginBtn);
        segmentedControl = (RadioGroup) findViewById(R.id.segmented_control);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ViewAnimUtils.activityExitTransitions(Register_Activity.this);
        finish();
    }

    private void getSchoolList() {
        progressDialog.show();
        Call<ResponseBody> getSchoolResponse = apiService.getSchoolList();
        getSchoolResponse.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    progressDialog.dismiss();
                    JSONObject jsonObject = new JSONObject(new String(response.body().string()));
                    if (jsonObject.optString("status_code").equalsIgnoreCase("200")) {
                        schoolIdList = new ArrayList<>();
                        schoolNameList = new ArrayList<>();

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            schoolNameList.add(jsonArray.optJSONObject(i).optString("name"));
                            schoolIdList.add(jsonArray.optJSONObject(i).optString("id"));
                        }
                    }
                    getCourseList();
                } catch (Exception e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                    getCourseList();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    private void getCourseList() {
        progressDialog.show();
        Call<ResponseBody> getSchoolResponse = apiService.getCourseList();
        getSchoolResponse.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    progressDialog.dismiss();
                    JSONObject jsonObject = new JSONObject(new String(response.body().string()));
                    if (jsonObject.optString("status_code").equalsIgnoreCase("200")) {
                        courseIdList = new ArrayList<>();
                        courseNameList = new ArrayList<>();
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            courseNameList.add(jsonArray.optJSONObject(i).optString("name"));
                            courseIdList.add(jsonArray.optJSONObject(i).optString("id"));
                        }
                    }
                } catch (Exception e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    private void mUserRegister() {
        progressDialog.show();
        try {
            int selectedId = segmentedControl.getCheckedRadioButtonId();
            opt1 = (RadioButton) findViewById(selectedId);
            if (opt1.getText().toString().equalsIgnoreCase("Male"))
            {
                gender = "1";
            } else
            {
                gender = "2";
            }

            Call<ResponseBody> getUserResponse = apiService.userRegister(userfirstEdittext.getText().toString(),
                    userlastEdittext.getText().toString(),
                    TextUtils.join(",",course_ids),
                    Georgeway_Application.getSharedPreferences().getString(AppUtils.FCM_TOKEN,""),
                    userEmailEdittext.getText().toString(),
                    userageEdittext.getText().toString(),
                    gender,userpasswordEdittext.getText().toString(),
                    schoolIds,
                    "Android");

            getUserResponse.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    progressDialog.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(new String(response.body().string()));
                        if (jsonObject.optString("status_code").equalsIgnoreCase("200")) {
                            updateProfileFirebase();
                            mShowDialog(Register_Activity.this, jsonObject.optString("success_message"));
                        } else {
                            AppUtils.mShowDialog(Register_Activity.this, jsonObject.optString("error_message"));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    progressDialog.dismiss();
                }
            });

        } catch (Exception e) {
            Log.e("success", "" + e.toString());
            e.printStackTrace();
            progressDialog.dismiss();

        }
    }
    private void mUserSocialRegister() {
        progressDialog.show();
        try {
            int selectedId = segmentedControl.getCheckedRadioButtonId();
            opt1 = (RadioButton) findViewById(selectedId);
            if (opt1.getText().toString().equalsIgnoreCase("Male")) {
                gender = "1";
            } else {
                gender = "2";
            }

            Call<ResponseBody> getUserResponse = apiService.socialRegister(userfirstEdittext.getText().toString(), userlastEdittext.getText().toString(), TextUtils.join(",",course_ids), Georgeway_Application.getSharedPreferences().getString(AppUtils.FCM_TOKEN,""), userEmailEdittext.getText().toString(), userageEdittext.getText().toString(), userpasswordEdittext.getText().toString(), gender, schoolIds, "Android",social_type,social_id);

            getUserResponse.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    progressDialog.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(new String(response.body().string()));
                        if (jsonObject.optString("status_code").equalsIgnoreCase("200")) {
                            updateProfileFirebase();
                            mShowDialog(Register_Activity.this, jsonObject.optString("success_message"));
                        } else {
                            AppUtils.mShowDialog(Register_Activity.this, jsonObject.optString("error_message"));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    progressDialog.dismiss();
                }
            });

        } catch (Exception e) {
            Log.e("success", "" + e.toString());
            e.printStackTrace();
            progressDialog.dismiss();

        }
    }
    void mShowDialog(AppCompatActivity appCompatActivity, String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(appCompatActivity);
        builder.setIcon(R.drawable.ic_launcher);

        builder.setTitle(appCompatActivity.getResources().getString(R.string.app_name));
        builder.setMessage(msg);


        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                finish();
                ViewAnimUtils.activityExitTransitions(Register_Activity.this);
            }
        });
        AlertDialog alertDialog = builder.create();
        int textColorId = appCompatActivity.getResources().getIdentifier("alertMessage", "id", "android");
        TextView textColor = (TextView) alertDialog.findViewById(textColorId);
        if (textColor != null) {
            textColor.setTextColor(Color.BLACK);
        }
        alertDialog.show();
    }

    private void updateProfileFirebase() {
        try {

            Call<ResponseBody> getUserResponse = apiService.updateFirebaseUserProfile(Georgeway_Application.getSharedPreferences().getString("userId", ""));

            getUserResponse.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                }
            });

        } catch (Exception e)
        {
            Log.e("success", "" + e.toString());
            e.printStackTrace();

        }

    }
}
