package com.georgewayTutor.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.georgewayTutor.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.MessageDigest;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;


/**
 * Created by jaikishang on 12/21/2016.
 */

public class AppUtils {

    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public final static String FCM_TOKEN = "_fcm_token";

    public int getColor(Context context, int id) {

        final int version = Build.VERSION.SDK_INT;
        if (version >= 23) {

            return ContextCompat.getColor(context, id);

        } else {

            return context.getResources().getColor(id);

        }

    }

    public static String getTimeStr(long time){
        Date date = new Date(time);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
        return simpleDateFormat.format(date);
    }


    /**
     * Check Network Connectivity
     *
     * @param context
     * @return
     */
    public static boolean isNetworkAvailable(Context context) {
        try {
            NetworkInfo networkInfo = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
            boolean connected = networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();
            return connected;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * hide soft input keyboard
     */
    public static void hideSoftKeyboard(Activity activity) {

        try {
            if (activity.getCurrentFocus() != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * hide soft input keyboard
     */
    public void showSoftKeyboard(Activity activity) {

        try {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    /**
     * @param mActivity
     * @param messageToShow
     */
    public void showMessageSnackBar(Activity mActivity, String messageToShow) {

        Toast.makeText(mActivity, messageToShow, Toast.LENGTH_SHORT).show();

    }
    public static boolean isValidLoginPassword(EditText edt_password, AppCompatActivity appCompatActivity) {
        boolean validation = true;
        String password = edt_password.getText().toString();
        if (password.trim().isEmpty()) {
            validation = false;

        }
        return validation;
    }

//

    /**
     * Setting the layout for arabic and english
     */
    //AppUtils.setDeviceLanguage(this);
    public static void setDeviceLanguage(Activity currentActivity) {

        /**
         * Detecting current selected language of device
         */
        String currentDeviceLanguage = Locale.getDefault().getDisplayLanguage();


        if (currentDeviceLanguage.equals("فارسی") || currentDeviceLanguage.equals("العربية")) {
            setLocale(currentActivity.getApplicationContext(), "ar");
        }
    }

    /**
     * this method identifies for selection of layout for specific language like Arabic
     *
     * @param context
     * @param lang
     */
    public static void setLocale(final Context context, final String lang) {
        final Locale loc = new Locale(lang);
        Locale.setDefault(loc);
        final Configuration cfg = new Configuration();
        cfg.locale = loc;
        context.getResources().updateConfiguration(cfg, null);
    }


    /**
     * Generate Hash Key
     *
     * @param appCompatActivity
     */

    public static void mGenerateHashKey(AppCompatActivity appCompatActivity) {
        try {
            PackageInfo info = appCompatActivity.getPackageManager().getPackageInfo("com.georgewayTutor", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void expand(final View v) {
        v.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? LinearLayout.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public static String mFilePath(String imageName) {
        return imageName.substring(imageName.lastIndexOf("/") + 1);
    }

    public static String mGetDate(Long timeStamp) {
        String dateFoemat = "";
        dateFoemat = new SimpleDateFormat("dd/MM/yyyy").format(new Date(timeStamp));

        return dateFoemat;
    }

    /**
     * valid email
     *
     * @param edtTxtEmailAddress
     * @return
     */
    public static boolean isValidEmailAddress(EditText edtTxtEmailAddress, AppCompatActivity appCompatActivity) {

        boolean validation = true;
        Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(EMAIL_PATTERN);
        String login_email_address = edtTxtEmailAddress.getText().toString();

        if (login_email_address.trim().isEmpty()) {

            validation = false;

        } else if (!EMAIL_ADDRESS_PATTERN.matcher(login_email_address).matches()) {

            validation = false;
        }

        return validation;

    }
    public static String mAddress(AppCompatActivity appCompatActivity, Double latitude, Double longitude)
    {
        Geocoder geocoder = new Geocoder(appCompatActivity, Locale.getDefault());

        // Address found using the Geocoder.
        List<Address> addresses = null;

        try {
            // Using getFromLocation() returns an array of Addresses for the area immediately
            // surrounding the given latitude and longitude. The results are a best guess and are
            // not guaranteed to be accurate.
            addresses = geocoder.getFromLocation(latitude,longitude,1);
        } catch (IOException ioException) {
            // Catch network or other I/O problems.
            Log.e("", "errorMessage", ioException);
        } catch (IllegalArgumentException illegalArgumentException) {
            // Catch invalid latitude or longitude values.
            Log.e("", "errorMessage "+ ". " +
                    "Latitude = " + latitude +
                    ", Longitude = " + longitude, illegalArgumentException);
        }
        ArrayList<String> addressFragments = new ArrayList<>();
        // Handle case where no address was found.
        if (addresses == null || addresses.size()  == 0) {
        } else {
            Address address = addresses.get(0);


            // Fetch the address lines using {@code getAddressLine},
            // join them, and send them to the thread. The {@link android.location.address}
            // class provides other options for fetching address details that you may prefer
            // to use. Here are some examples:
            // getLocality() ("Mountain View", for example)
            // getAdminArea() ("CA", for example)
            // getPostalCode() ("94043", for example)
            // getCountryCode() ("US", for example)
            // getCountryName() ("United States", for example)
            for(int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                addressFragments.add(address.getAddressLine(i));
            }

            Log.e("User Address"," "+ TextUtils.join(System.getProperty(","), addressFragments));

        }
        return TextUtils.join(System.getProperty(","), addressFragments).replaceAll("null"," ");
    }

    public static String mDate(String stringDate)
    {
        String mDate = "";
        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(stringDate);
            mDate = new SimpleDateFormat("MMM, dd, yyyy hh:mm a").format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return  mDate;
    }

    public static String mDateVendor(String stringDate)
    {
        String mDate = "";
        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(stringDate);
            mDate = new SimpleDateFormat("dd-MMMM-yyyy hh:mm a").format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return  mDate;
    }

    public static String mDate3(String stringDate)
    {
        String mDate = "";
        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(stringDate);
            mDate = new SimpleDateFormat("EEEE MMM dd, yyyy hh:mm a").format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return  mDate;
    }
    public static String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    public static String mConvertDate(String stringDate)
    {
        String mDate = "";
        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd").parse(stringDate);
            mDate = new SimpleDateFormat("EEEE dd MMMM").format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return  mDate;
    }
    public static String mConvertDate1(String stringDate)
    {
        String mDate = "";
        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd").parse(stringDate);
            mDate = new SimpleDateFormat("dd, MMM yyyy").format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return  mDate;
    }

    public static String mConvertDate12(String stringDate)
    {
        String mDate = "";
        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd").parse(stringDate);
            mDate = new SimpleDateFormat("dd-MMM-yyyy").format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return  mDate;
    }

    public static String mDate1(String stringDate)
    {
        String mDate = "";
        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd").parse(stringDate);
            mDate = new SimpleDateFormat("dd MMMM, yyyy").format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return  mDate;
    }

    public static String mConvertTime(String stringDate)
    {
        String mDate = "";
        try {
            Date date = new SimpleDateFormat("hh:mm:ss").parse(stringDate);
            mDate = new SimpleDateFormat("hh:mm a").format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return  mDate;
    }
//    public static void disableAppSwitchSignatureVerification(boolean disable) {
//        SignatureVerification.sEnableSignatureVerification = !disable;
//    }
    public static void mShowDialog(AppCompatActivity appCompatActivity, String msg) {
            AlertDialog.Builder builder = new AlertDialog.Builder(appCompatActivity);
            builder.setIcon(R.drawable.ic_launcher);

            builder.setTitle(appCompatActivity.getResources().getString(R.string.app_name));
            builder.setMessage(msg);


            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            AlertDialog alertDialog = builder.create();
            int textColorId = appCompatActivity.getResources().getIdentifier("alertMessage", "id", "android");
            TextView textColor = (TextView) alertDialog.findViewById(textColorId);
            if (textColor != null) {
                textColor.setTextColor(Color.BLACK);
            }
                alertDialog.show();
        }

        public static ProgressDialog getProgressDialog(AppCompatActivity appCompatActivity)
    {
        ProgressDialog progressDialog = new ProgressDialog(appCompatActivity);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

    public static HashMap jsonToMap(String t) throws JSONException {

        HashMap<String, String> map = new HashMap<String, String>();
        JSONObject jObject = new JSONObject(t);
        Iterator<?> keys = jObject.keys();

        while( keys.hasNext() ){
            String key = (String)keys.next();
            String value = jObject.getString(key);
            map.put(key, value);

        }

        System.out.println("json : "+jObject);
        System.out.println("map : "+map);

        return  map;
    }
    public static String dateTime(Long dateTimestamp)
    {
        String timee= "";
        Timestamp timestamp = new Timestamp(dateTimestamp);
        Date date = new Date(timestamp.getTime());

        // S is the millisecond
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
        timee = simpleDateFormat.format(timestamp);
        System.out.println(simpleDateFormat.format(timestamp));
        System.out.println(simpleDateFormat.format(date));
        return getFormattedDate(dateTimestamp);
    }
    public static Map<String, Object> jsonToMap(JSONObject json) throws JSONException {
        Map<String, Object> retMap = new HashMap<String, Object>();

        if(json != JSONObject.NULL) {
            retMap = toMap(json);
        }
        return retMap;
    }

    public static Map<String, Object> toMap(JSONObject object) throws JSONException {
        Map<String, Object> map = new HashMap<String, Object>();

        Iterator<String> keysItr = object.keys();
        while(keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);

            if(value instanceof JSONArray) {
                value = toList((JSONArray) value);
            }

            else if(value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);
        }
        return map;
    }

    public static List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<Object>();
        for(int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if(value instanceof JSONArray) {
                value = toList((JSONArray) value);
            }

            else if(value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }

    public static String getFormattedDate(long smsTimeInMilis) {
//        Calendar smsTime = Calendar.getInstance();
//        smsTime.setTimeInMillis(smsTimeInMilis);
//
//        Calendar now = Calendar.getInstance();
//
//        final String timeFormatString = "h:mm aa";
//        final String dateTimeFormatString = "EEEE, MMMM d, h:mm aa";
//        final long HOURS = 60 * 60 * 60;
//        if (now.get(Calendar.DATE) == smsTime.get(Calendar.DATE) ) {
//            return "" + DateFormat.format(timeFormatString, smsTime);
//        } else if (now.get(Calendar.DATE) - smsTime.get(Calendar.DATE) == 1  ){
//            return "Yesterday " + DateFormat.format(timeFormatString, smsTime);
//        } else if (now.get(Calendar.YEAR) == smsTime.get(Calendar.YEAR)) {
//            return DateFormat.format(dateTimeFormatString, smsTime).toString();
//        } else {
//            return DateFormat.format("MMMM dd yyyy, h:mm aa", smsTime).toString();
//        }

        return getTimeAgo(smsTimeInMilis);
    }
    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;
    public static String getTimeAgo(long time) {
        if (time < 1000000000000L) {
            // if timestamp given in seconds, convert to millis
            time *= 1000;
        }

        long now = System.currentTimeMillis();
        if (time > now || time <= 0) {
            return null;
        }

        // TODO: localize
        final long diff = now - time;
        if (diff < MINUTE_MILLIS) {
            return "just now";
        } else if (diff < 2 * MINUTE_MILLIS) {
            return "a minute ago";
        } else if (diff < 50 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " minutes ago";
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "an hour ago";
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + " hours ago";
        } else if (diff < 48 * HOUR_MILLIS) {
            return "yesterday";
        } else {
            return diff / DAY_MILLIS + " days ago";
        }
    }

}
