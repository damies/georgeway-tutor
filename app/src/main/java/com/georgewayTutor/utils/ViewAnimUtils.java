package com.georgewayTutor.utils;


import android.support.v7.app.AppCompatActivity;

import com.georgewayTutor.R;


public class ViewAnimUtils {
    /**
     * this method is used for animating activity on start with slide in to left
     * @param mActivity
     */
    public  static void activityEnterTransitions(AppCompatActivity mActivity){
        mActivity.overridePendingTransition(R.anim.anim_slide_in, R.anim.anim_slide_out);
    }

    /**
     * this method is used for animating activity on finish with slide out to right
     * @param mActivity
     */
    public  static void activityExitTransitions(AppCompatActivity mActivity){
        mActivity.overridePendingTransition(R.anim.anim_slide_enter, R.anim.anim_slide_exit);
    }

}