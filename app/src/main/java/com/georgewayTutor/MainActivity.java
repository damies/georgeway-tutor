package com.georgewayTutor;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.georgewayTutor.Models.s.ChatInfoModel;
import com.georgewayTutor.Models.s.ChatModel;
import com.georgewayTutor.api.APIService;
import com.georgewayTutor.fragments.Account_Fragment;
import com.georgewayTutor.fragments.Earning_Fragment;
import com.georgewayTutor.fragments.Home_Fragment;
import com.georgewayTutor.fragments.OngoingClass;
import com.georgewayTutor.utils.CircularImageView;
import com.georgewayTutor.utils.ViewAnimUtils;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    boolean isLive;
    private TextView mTextMessage;
    private SwitchCompat switchForActionBar;
    private DatabaseReference referenceMyCurrentRequests, chatDatabase, tutorChatDatabse, userChatDatabase, userOnline, referenceMyself;
    private BottomSheetDialog bottomSheetDialog;
    public static boolean currentChat = false;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    mTextMessage.setText(R.string.title_home);
                    replaceFragment(Account_Fragment.getInstance(aClass,currentChat), "Account");
                    return true;
                case R.id.navigation_dashboard:
                    mTextMessage.setText(R.string.title_dashboard);
                    replaceFragment(new Earning_Fragment(MainActivity.this), "Earning");
                    return true;
//                case R.id.navigation_notifications:
//                    mTextMessage.setText(R.string.title_notifications);
//                    replaceFragment(new Account_Fragment(MainActivity.this), "Account");
//                    return true;
            }
            return false;
        }
    };
    private TextView userNameTxt;
    private RelativeLayout schoolView;
    private TextView schoolTitle;
    private TextView schoolNameTxt;
    private TextView courseTitle;
    private TextView courseNameTxt;
    private TextView timerTxt;
    private ProgressBar progressBarCircle;
    private Button requestDonebtn;
    private Button requestCancelbtn;
    private String current_requst_key = "", userrequestId = "";
    private CircularImageView userImageView;
    private JSONObject userDetailsJson;
    private JSONObject userDetailsMapJson;
    private JSONObject tutorDetailsMapJson;
    private static long requestTimerStartValue=0;
    private TextView logout_txt;
    private DrawerLayout mDrawerLayout;
    private NavigationView nav_view;
    private TextView schoolName_txt;
    private JSONObject currenChatValue;

    private enum TimerStatus {
        STARTED,
        STOPPED
    }

    private TimerStatus timerStatus = TimerStatus.STOPPED;
    private CountDownTimer countDownTimer;
    private ImageView menuImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        isLive = true;

        bottomSheetDialog = new BottomSheetDialog(MainActivity.this);
        referenceMyself = FirebaseDatabase.getInstance().getReference().child("tutors").child(Georgeway_Application.getSharedPreferences().getString("userId", ""));
        referenceMyCurrentRequests = FirebaseDatabase.getInstance().getReference("tutors").child(Georgeway_Application.getSharedPreferences().getString("userId", "")).child("current_requests");
        tutorChatDatabse = FirebaseDatabase.getInstance().getReference("tutors").child(Georgeway_Application.getSharedPreferences().getString("userId", "")).child("chat_rooms");
        chatDatabase = FirebaseDatabase.getInstance().getReference("chats");
        userChatDatabase = FirebaseDatabase.getInstance().getReference("users");
        userOnline = FirebaseDatabase.getInstance().getReference("tutors").child(Georgeway_Application.getSharedPreferences().getString("userId", "")).child("is_online");
        switchForActionBar = (SwitchCompat) findViewById(R.id.switchForActionBar);
        mTextMessage = (TextView) findViewById(R.id.title_txt);
        logout_txt = (TextView) findViewById(R.id.logout_txt);
        menuImage = (ImageView) findViewById(R.id.menuImage);
        nav_view = (NavigationView) findViewById(R.id.nav_view);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.viewpager, Account_Fragment.getInstance(aClass,currentChat), "Account").commit();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        bottomSheetDialog.setContentView(R.layout.new_request_dialog);
        bottomSheetDialog.setCanceledOnTouchOutside(false);
        bottomSheetDialog.setCancelable(false);
        initView(bottomSheetDialog);

        switchForActionBar.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    setOnline("" + b);
                } else {
                    setOnline("" + b);
                }
            }
        });
        logout_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setIcon(R.drawable.ic_launcher);
                builder.setTitle(getResources().getString(R.string.app_name));
                builder.setMessage("Are you sure, You want to logout?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        setOnline(""+false);
                        Georgeway_Application.getSharedPreferences().edit().putString("userId","")
                                .putString("firstName","")
                                .putString("lastName","")
                                .putString("image","")
                                .putString("isLogin","false").apply();
                        startActivity(new Intent(MainActivity.this, Login_Activity.class));
                        finishAffinity();
                        ViewAnimUtils.activityEnterTransitions(MainActivity.this);
                    }
                });

                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });
        userOnline.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null) {
                    if (dataSnapshot.getValue() instanceof String) {
                        String is_online = "" + dataSnapshot.getValue();
                        if (is_online.equalsIgnoreCase("true")) {
                            switchForActionBar.setChecked(true);
                        } else {
                            switchForActionBar.setChecked(false);
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



        referenceMyCurrentRequests.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot != null && dataSnapshot.getValue()instanceof HashMap) {
                    Log.e("TAG ", "onChildAdded" + dataSnapshot.getValue());
                    HashMap hashMap = (HashMap) dataSnapshot.getValue();
                    if (hashMap!=null){
                        Long timeStamp = null;
                        try{
                            timeStamp = (long) hashMap.get("time_stamp");
                            if (timeStamp!=null && timeStamp!=0){
                                long diff = System.currentTimeMillis() - timeStamp;
                                if (diff<30000){
                                    requestTimerStartValue = 30000-diff;
                                }else{
                                    requestTimerStartValue = 0;
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        /*if (timeStamp + 28 > System.currentTimeMillis()){*/

                            current_requst_key = "" + dataSnapshot.getKey();
                            userDetailsJson = new JSONObject(hashMap);
                            Log.e("jsonObject", " " + userDetailsJson);

                            userNameTxt.setText(userDetailsJson.optString("first_name") + " " + userDetailsJson.optString("last_name"));
                            schoolName_txt.setText(userDetailsJson.optString("school"));
                            String image = userDetailsJson.optString("image");
                            userrequestId = userDetailsJson.optString("id");
                            HashMap userDetailsMap = new HashMap();
                            userDetailsMap.put("first_name", userDetailsJson.optString("first_name"));
                            userDetailsMap.put("last_name", userDetailsJson.optString("last_name"));
                            userDetailsMap.put("image", userDetailsJson.optString("image"));
                            userDetailsMap.put("user_id", userDetailsJson.optString("id"));
                            userDetailsMap.put("is_active", "true");
                            userDetailsMap.put("tutor_id", Georgeway_Application.getSharedPreferences().getString("userId", ""));
                            userDetailsMapJson = new JSONObject(userDetailsMap);

                            HashMap tutorMap = new HashMap();
                            tutorMap.put("first_name", Georgeway_Application.getSharedPreferences().getString("firstName", ""));
                            tutorMap.put("last_name", Georgeway_Application.getSharedPreferences().getString("lastName", ""));
                            tutorMap.put("image", Georgeway_Application.getSharedPreferences().getString("image", ""));
                            tutorMap.put("user_id", userDetailsJson.optString("id"));
                            tutorMap.put("is_active", "true");
                            tutorMap.put("tutor_id", Georgeway_Application.getSharedPreferences().getString("userId", ""));
                            tutorDetailsMapJson = new JSONObject(tutorMap);

                            if (!TextUtils.isEmpty(image) && image.startsWith("https:")) {
                                ImageLoader.getInstance().displayImage(image, userImageView, new ImageLoadingListener() {
                                    @Override
                                    public void onLoadingStarted(String imageUri, View view) {
                                    }

                                    @Override
                                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                                        userImageView.setImageResource(R.drawable.user);
                                    }

                                    @Override
                                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                        userImageView.setImageBitmap(loadedImage);
                                    }

                                    @Override
                                    public void onLoadingCancelled(String imageUri, View view) {
                                        userImageView.setImageResource(R.drawable.user);

                                    }
                                });

                            } else {
                                ImageLoader.getInstance().displayImage(APIService.API_BASE_URL + image, userImageView, new ImageLoadingListener() {
                                    @Override
                                    public void onLoadingStarted(String imageUri, View view) {
                                    }

                                    @Override
                                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                                        userImageView.setImageResource(R.drawable.user);
                                    }

                                    @Override
                                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                        userImageView.setImageBitmap(loadedImage);
                                    }

                                    @Override
                                    public void onLoadingCancelled(String imageUri, View view) {
                                        userImageView.setImageResource(R.drawable.user);

                                    }
                                });
                            }

                            if (isLive && !bottomSheetDialog.isShowing()) {
                                bottomSheetDialog.show();
                                toggleRequestTimer(progressBarCircle, timerTxt);
                            }
                        }else{
                            /*String key = dataSnapshot.getKey();
                            referenceMyCurrentRequests.child(key).setValue(null);*/
                        }
                    }
                /*}*/
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                //Toast.makeText(MainActivity.this, "onChildChanged", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                if (isLive && bottomSheetDialog!=null && bottomSheetDialog.isShowing()){
                    bottomSheetDialog.dismiss();
                    toggleRequestTimer(progressBarCircle, timerTxt);
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                //Toast.makeText(MainActivity.this, "onChildMoved", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
               // Toast.makeText(MainActivity.this, "onCancelled", Toast.LENGTH_SHORT).show();
            }

        });
        requestDonebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    ChatModel chatModel = new ChatModel("Hi", Georgeway_Application.getSharedPreferences().getString("userId", ""), ServerValue.TIMESTAMP, "T", "", "T");
//                    tutorChatDatabse.child(current_requst_key).setValue(AppUtils.jsonToMap(userDetailsMapJson));
                    chatDatabase.child(current_requst_key).push().setValue(chatModel);
                    tutorChatDatabse.child(current_requst_key).setValue(new ChatInfoModel(chatModel, "false", ServerValue.TIMESTAMP, "true", userDetailsJson.optString("first_name"), userDetailsJson.optString("last_name"), userDetailsJson.optString("image"), Georgeway_Application.getSharedPreferences().getString("userId", ""), userrequestId));

                    userChatDatabase.child(userrequestId + "/chat_rooms/" + current_requst_key).setValue(new ChatInfoModel(chatModel, "true", ServerValue.TIMESTAMP, "true", tutorDetailsMapJson.optString("first_name"), tutorDetailsMapJson.optString("last_name"), tutorDetailsMapJson.optString("image"), Georgeway_Application.getSharedPreferences().getString("userId", ""), userrequestId));

                    bottomSheetDialog.dismiss();
                    toggleRequestTimer(progressBarCircle, timerTxt);

                    startActivity(new Intent(MainActivity.this, Chat_Activity.class).putExtra("name", userDetailsMapJson.optString("first_name") + " " + userDetailsMapJson.optString("last_name"))
                            .putExtra("image", userDetailsMapJson.getString("image")).putExtra("chatkey", current_requst_key).putExtra("userId", userrequestId));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        requestCancelbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                referenceMyCurrentRequests.child(current_requst_key).setValue(null);
                referenceMyself.child("total_request_count").setValue(0);

                bottomSheetDialog.dismiss();
                toggleRequestTimer(progressBarCircle, timerTxt);
            }
        });

        menuImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawerLayout.openDrawer(Gravity.START);
            }
        });

        nav_view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        mTextMessage.setText(R.string.title_home);
                        replaceFragment(new Home_Fragment(MainActivity.this), "Home");
                        break;
                    case R.id.navigation_dashboard:
                        mTextMessage.setText(R.string.title_dashboard);
                        replaceFragment(new Earning_Fragment(MainActivity.this), "Earning");
                        break;
                    case R.id.navigation_notifications:
                        mTextMessage.setText(R.string.title_notifications);
                        replaceFragment(Account_Fragment.getInstance(aClass,currentChat), "Account");
                        break;
                }
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
                return true;
            }
        });

        tutorChatDatabse.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot != null && dataSnapshot.getValue() instanceof HashMap) {
                    if (dataSnapshot.getValue() != null && dataSnapshot.getValue() instanceof HashMap) {
                        HashMap hashMap = (HashMap) dataSnapshot.getValue();
                        if (hashMap != null) {
                            String currStatus = (String) hashMap.get("is_active");
                            if (currStatus!=null && currStatus.equalsIgnoreCase("true")) {
                                current_requst_key = dataSnapshot.getKey();
                                currenChatValue = new JSONObject(hashMap);
                                currentChat = true;
                                showOngoingLayout(true);
                            }else{
                                currentChat = false;
                                showOngoingLayout(false);
                            }
                        } else {
                            currentChat = false;
                            showOngoingLayout(false);
                        }
                    }
                    Log.e("onChildAdded", " " + dataSnapshot.getValue());
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot != null && dataSnapshot.getValue() instanceof HashMap) {
                    Log.e("onChildChanged", " " + dataSnapshot.getValue());
                    if (dataSnapshot.getValue() != null) {
                        current_requst_key = dataSnapshot.getKey();
                        currentChat = true;
                        showOngoingLayout(true);
                        HashMap hashMap = (HashMap) dataSnapshot.getValue();
                        currenChatValue = new JSONObject(hashMap);
                    } else {
                        currentChat = false;
                        showOngoingLayout(false);
                    }
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null) {
                    Log.e("onChildRemoved", " " + dataSnapshot.getValue());
                    current_requst_key = "";
                    currentChat = false;
                    showOngoingLayout(false);
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        showOngoingLayout(currentChat);
    }



    private void replaceFragment(Fragment newFragment, String tag) {
        if (tag.equalsIgnoreCase("home")) {
            switchForActionBar.setVisibility(View.VISIBLE);
            logout_txt.setVisibility(View.GONE);
        } else {
            switchForActionBar.setVisibility(View.VISIBLE);
            logout_txt.setVisibility(View.VISIBLE);
        }
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.viewpager, newFragment, tag)
                .commit();

    }

    @Override
    protected void onDestroy() {
        isLive =false;
//        setOnline("" + false);
        super.onDestroy();
        Log.e("onDestory", "called");
    }

    @Override
    protected void onStop() {
        super.onStop();
//       setOnline(""+false);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //    setOnline("" + true);
    }


    @Override
    protected void onResume() {
        super.onResume();
        //    setOnline("" + true);
    }

    private void initView(BottomSheetDialog bottomSheetDialog) {
        userNameTxt = (TextView) bottomSheetDialog.findViewById(R.id.userName_txt);
        schoolName_txt = (TextView) bottomSheetDialog.findViewById(R.id.schoolName_txt);
        schoolView = (RelativeLayout) bottomSheetDialog.findViewById(R.id.schoolView);
        schoolTitle = (TextView) bottomSheetDialog.findViewById(R.id.schoolTitle);
        schoolNameTxt = (TextView) bottomSheetDialog.findViewById(R.id.schoolName_txt);
        courseTitle = (TextView) bottomSheetDialog.findViewById(R.id.courseTitle);
        courseNameTxt = (TextView) bottomSheetDialog.findViewById(R.id.courseName_txt);
        timerTxt = (TextView) bottomSheetDialog.findViewById(R.id.timerTxt);
        progressBarCircle = (ProgressBar) bottomSheetDialog.findViewById(R.id.progressBarCircle);
        requestDonebtn = (Button) bottomSheetDialog.findViewById(R.id.requestDonebtn);
        requestCancelbtn = (Button) bottomSheetDialog.findViewById(R.id.requestCancelbtn);
        userImageView = (CircularImageView) bottomSheetDialog.findViewById(R.id.userImageView);
    }

    /**
     * method to start and stop count down timer
     *
     * @param progressBarCircle
     * @param timerTxt
     */
    private void toggleRequestTimer(ProgressBar progressBarCircle, TextView timerTxt) {
        if (timerStatus == TimerStatus.STOPPED) {

            // call to initialize the timer values
            setTimerValues();
            // call to initialize the progress bar values
            setProgressBarValues(progressBarCircle);
            // showing the reset icon
            // changing the timer status to started
            timerStatus = TimerStatus.STARTED;
            // call to start the count down timer
            startCountDownTimer(progressBarCircle, timerTxt);
        } else {

            // changing the timer status to stopped
            timerStatus = TimerStatus.STOPPED;
            stopCountDownTimer();
        }
    }

    /**
     * method to initialize the values for count down timer
     */
    private void setTimerValues() {

        // assigning values after converting to milliseconds
        if (requestTimerStartValue==0)
        requestTimerStartValue = 30000;
    }

    /**
     * method to start count down timer
     *
     * @param progressBarCircle
     * @param textViewTime
     */
    private void startCountDownTimer(final ProgressBar progressBarCircle, final TextView textViewTime) {

        countDownTimer = new CountDownTimer(requestTimerStartValue, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

                textViewTime.setText(hmsTimeFormatter(millisUntilFinished));

                progressBarCircle.setProgress((int) (millisUntilFinished / 1000));

            }

            @Override
            public void onFinish() {
                if (!isLive)return;

                textViewTime.setText(hmsTimeFormatter(requestTimerStartValue));
                // call to initialize the progress bar values
                setProgressBarValues(progressBarCircle);
                // hiding the reset icon
                // changing the timer status to stopped
                timerStatus = TimerStatus.STOPPED;
                referenceMyCurrentRequests.child(current_requst_key).setValue(null);
                referenceMyself.child("total_request_count").setValue(0);
                if (isLive&&bottomSheetDialog.isShowing())
                 bottomSheetDialog.dismiss();

            }

        }.start();
        countDownTimer.start();
    }

    /**
     * method to stop count down timer
     */
    private void stopCountDownTimer() {
        countDownTimer.cancel();
    }

    /**
     * method to set circular progress bar values
     *
     * @param progressBarCircle
     */
    private void setProgressBarValues(ProgressBar progressBarCircle) {

        progressBarCircle.setMax((int) requestTimerStartValue / 1000);
        progressBarCircle.setProgress((int) requestTimerStartValue / 1000);
    }

    /* @param milliSeconds
    * @return mm:ss time formatted string
    */
    private String hmsTimeFormatter(long milliSeconds) {

        String hms = String.format("%02d",
                TimeUnit.MILLISECONDS.toSeconds(milliSeconds) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milliSeconds)));
        return hms;
    }

    private void setOnline(String flag) {
        userOnline.setValue(flag);
    }

    private OngoingClass aClass = new OngoingClass() {
        @Override
        public void onClickOnGoing() {
            if (currentChat) {
                try {
                    startActivity(new Intent(MainActivity.this, Chat_Activity.class).putExtra("name", currenChatValue.optString("first_name") + " " + currenChatValue.optString("last_name")).putExtra("image", currenChatValue.getString("image")).putExtra("chatkey", current_requst_key).putExtra("userId", currenChatValue.optString("user_id")));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(MainActivity.this,"Sorry, no active session available",Toast.LENGTH_LONG).show();
            }
        }
    };

    public void showOngoingLayout(boolean show){
        Account_Fragment frag = (Account_Fragment) getSupportFragmentManager().findFragmentByTag("Account");
        if (frag!=null){
            frag.showOngoingLayout(show);
        }

    }


}
