package com.georgewayTutor.api;


import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by param .
 */
public class APIService {
    public APIService() {

    }

    public static final String API_BASE_URL = "https://georgewayglobal.com/";
    public static final String URL_TERMS_AND_CONDITIONS = "https://georgewayglobal.com/georgeway_website/terms_and_conditions";
    public static final String URL_HELP = "https://georgewayglobal.com/georgeway_website/contact";

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit retrofit = builder.build();


    public static <S> S createService(Class<S> serviceClass) {
      //  AuthenticationInterceptor interceptor = new AuthenticationInterceptor("");

      //  httpClient.addInterceptor(interceptor);
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addInterceptor(logging);
        httpClient.connectTimeout(1000, TimeUnit.SECONDS);
        httpClient.readTimeout(1000, TimeUnit.SECONDS);
        httpClient.writeTimeout(1000, TimeUnit.SECONDS);
        builder.client(httpClient.build());
        retrofit = builder.build();

        return retrofit.create(serviceClass);
    }

    public static class AuthenticationInterceptor implements Interceptor {

        private String uuId;

        public AuthenticationInterceptor(String uuId) {
            this.uuId = uuId;
        }

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request original = chain.request();

            Request.Builder builder = original.newBuilder()
                    .header("Content-Type", "charset=UTF-8")
                    .header("Accept-Language", "hi")
                    .header("x-UUID","8dcec22fa307b1dd")
                    .header("Accept", "application/json; version=2.0");


            Request request = builder.build();
            return chain.proceed(request);
        }
    }
}
