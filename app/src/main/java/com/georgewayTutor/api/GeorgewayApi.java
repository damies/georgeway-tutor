package com.georgewayTutor.api;

import com.georgewayTutor.Models.s.history.PaymentHistoryResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Url;


/**
 * Created by jai
 */
public interface GeorgewayApi {
    @GET("/tutor/school_list.php")
    Call<ResponseBody> getSchoolList();

    @GET("/tutor/course_list.php")
    Call<ResponseBody> getCourseList();

    @POST("/tutor/registration.php")
    @FormUrlEncoded
    Call<ResponseBody> userRegister(@Field("first_name") String first_name,@Field("last_name") String last_name,@Field("course_ids") String course_ids,@Field("device_token") String device_token,
        @Field("email") String email,@Field("age") String age,@Field("gender") String gender,@Field("password") String passowrd,@Field("school_id") String school_id,@Field("device_name") String device_name);

    @POST("/tutor/login.php")
    @FormUrlEncoded
    Call<ResponseBody> userLogin(@Field("device_token") String device_token,
                                 @Field("email") String email,
                                 @Field("password") String password,
                                 @Field("device_name") String device_name,
                                 @Field("device_type") String device_type);

    @POST("/tutor/forget_password.php")
    @FormUrlEncoded
    Call<ResponseBody> userForgotpassword(@Field("email") String email);

    @POST("/tutor/add_paypal_email_id.php")
    @FormUrlEncoded
    Call<ResponseBody> addPaypayID(@Field("paypal_email_id") String paypal_email_id,@Field("tutorid") String tutorid);


    @POST("/tutor/get_tutor.php")
    @FormUrlEncoded
    Call<ResponseBody> getUserProfile(@Field("id") String id);

    @POST("/tutor/payment_history.php")
    @FormUrlEncoded
    Call<PaymentHistoryResponse> getPaymentHistory(@Field("tutorid") String tutorid);


    @GET
    Call<ResponseBody> updateFirebaseUserProfile(@Url String id);

    @POST("/tutor/social_registration.php")
    @FormUrlEncoded
    Call<ResponseBody> socialRegister(@Field("first_name") String first_name,@Field("last_name") String last_name,@Field("course_ids") String course_ids,@Field("device_token") String device_token,
                                   @Field("email") String email,@Field("age") String age,@Field("gender") String gender,@Field("password") String passowrd,@Field("school_id") String school_id,@Field("device_name") String device_name,@Field("social_type") String social_type,@Field("social_id") String social_id);

    @POST("/tutor/social_login.php")
    @FormUrlEncoded
    Call<ResponseBody> socialLogin(@Field("'device_token") String device_token,@Field("'device_name") String device_name,@Field("device_type") String device_type,@Field("social_type") String social_type,@Field("email") String email,@Field("social_id") String social_id);


    @POST("/tutor/edit.php")
    @Multipart
    Call<ResponseBody> userDetailUpdate(@Part("first_name") RequestBody first_name, @Part("last_name") RequestBody last_name, @Part("email") RequestBody email, @Part("age") RequestBody age, @Part("gender") RequestBody gender, @Part("school_id") RequestBody school_id, @Part("course_ids") RequestBody course_ids, @Part("tutorid") RequestBody tutorid, @Part("about_us") RequestBody about_us, @Part MultipartBody.Part file);


//    @POST("/tutor/edit.php")
//    @FormUrlEncoded
//    Call<ResponseBody> userDetailUpdate(@Field("first_name") String first_name, @Field("last_name") String last_name, @Field("email") String email, @Field("age") String age, @Field("gender") String gender, @Part("school_id") RequestBody school_id, @Part("'course_ids") RequestBody course_ids, @Part("tutorid") RequestBody tutorid, @Part("about_us") RequestBody about_us, @Part MultipartBody.Part file);

}
