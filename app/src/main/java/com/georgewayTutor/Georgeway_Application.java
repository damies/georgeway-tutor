package com.georgewayTutor;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.StrictMode;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;


/**
 * Created by Jai Gupta on 7/30/2016.
 */
public class Georgeway_Application extends Application implements Thread.UncaughtExceptionHandler {


    private static Georgeway_Application mInstance;
    private static SharedPreferences sharedPref_respond;

    private Thread.UncaughtExceptionHandler mDefaultExceptionHandler;
    /**
     * {@inheritDoc}
     */
    @Override
    public void onCreate() {

        super.onCreate();
        initSharedPreferences();
        mInstance = this;
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
//        DeveloperTools.setup(this);

        mDefaultExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(this);

        ImageLoader.getInstance().init(getintiImageLoaderConfig(mInstance));

    }

    /**
     * INITIALIZATION SharedPreferences
     */

    private void initSharedPreferences() {
        try {
            sharedPref_respond = getApplicationContext().getSharedPreferences("sharedPref", Context.MODE_PRIVATE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * used to get instance globally of SharedPreferences
     */

    public static synchronized SharedPreferences getSharedPreferences() {
        return sharedPref_respond;
    }

    /*
    * Declear ImageLoaderConfiguraton
    * */

    public static ImageLoaderConfiguration getintiImageLoaderConfig(Context context) {
        // TODO Auto-generated method stub

//        DisplayImageOptions defaultOptions = intitOptions();

        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);

        return config.build();
    }

  /*
    * Declear DisplayImageOptions
    * */

    public static DisplayImageOptions intitOptions() {
        // TODO Auto-generated method stub
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheOnDisk(true).cacheInMemory(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .showImageOnFail(R.mipmap.app_icon)
                .showImageForEmptyUri(R.mipmap.app_icon)
                .considerExifParams(true)
                .displayer(new FadeInBitmapDisplayer(300)).build();
        return defaultOptions;
    }
 /*
    * Declear DisplayImageOptions
    * */

    public static DisplayImageOptions intitOptions1() {
        // TODO Auto-generated method stub
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheOnDisk(true).cacheInMemory(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .showImageOnFail(R.drawable.img_placeholder)
                .showImageForEmptyUri(R.drawable.img_placeholder)
                .showImageOnLoading(R.drawable.img_placeholder)
                .considerExifParams(true)
                .displayer(new RoundedBitmapDisplayer((int) 27.5f)).build();
        return defaultOptions;
    }
    @Override
    public void uncaughtException(Thread thread, Throwable ex) {

        mDefaultExceptionHandler.uncaughtException(thread, ex);
    }

}

