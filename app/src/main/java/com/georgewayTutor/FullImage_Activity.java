package com.georgewayTutor;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.georgewayTutor.utils.ZoomableImageView;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by JAI on 3/13/2018.
 */

public class FullImage_Activity extends AppCompatActivity {
    private ZoomableImageView zoomImage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.full_image_view);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initView();
        if (getIntent()!=null)
        {
            if (getIntent().getStringExtra("path")!=null)
            {
                ImageLoader.getInstance().displayImage(getIntent().getStringExtra("path"),zoomImage,Georgeway_Application.intitOptions());
            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void initView() {
        zoomImage = (ZoomableImageView) findViewById(R.id.zoomImage);
    }
}
