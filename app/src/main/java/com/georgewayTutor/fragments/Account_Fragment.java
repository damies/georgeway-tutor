package com.georgewayTutor.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.georgewayTutor.Georgeway_Application;
import com.georgewayTutor.R;
import com.georgewayTutor.TermsAndConditionsActivity;
import com.georgewayTutor.UpdateProfile_Acitvity;
import com.georgewayTutor.api.APIService;
import com.georgewayTutor.api.GeorgewayApi;
import com.georgewayTutor.utils.AppUtils;
import com.georgewayTutor.utils.CircularImageView;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by JAI on 1/13/2018.
 */


public class Account_Fragment extends Fragment {
    private GeorgewayApi apiService;
    private AppCompatActivity appCompatActivity;
    private RelativeLayout cardView;
    private ImageView updateImg;
    private LinearLayout layout;
    private TextView userMobileTxt;
    private TextView userEmailTxt;
    private TextView privacyTxt;
    private TextView helpTxt;
    private CircularImageView userImageView;
    private TextView userNameTxt;
    private TextView useransTxt, totaltimetxt, totalEraing, addPaymentInfotxt;
    private RatingBar tutorrating;
    private ProgressDialog progressDialog;
    private LinearLayout ongoingLayout;
    private OngoingClass aClass;
    private boolean isAnyChatLive;

    public Account_Fragment (){}

    public static Account_Fragment getInstance(OngoingClass ongoingClass,boolean isAnyChatLive){
        Account_Fragment frag = new Account_Fragment();
        frag.setOngoingClassListener(ongoingClass);
        Bundle b = new Bundle();
        b.putBoolean("isAnyChatLive",isAnyChatLive);
        frag.setArguments(b);
        return frag;
    }

    public void setOngoingClassListener(OngoingClass ongoingClassListener){
        this.aClass = ongoingClassListener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle b = getArguments();
        if (b!=null){
            this.isAnyChatLive = b.getBoolean("isAnyChatLive");
        }

        this.appCompatActivity = (AppCompatActivity) getActivity();
        apiService = APIService.createService(GeorgewayApi.class);
        progressDialog = AppUtils.getProgressDialog(appCompatActivity);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.account_fragment, container, false);

        initView(view);

        updateImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(appCompatActivity, UpdateProfile_Acitvity.class));
            }
        });
        addPaymentInfotxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                forgotPasswordDialog();
            }
        });
        ongoingLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                aClass.onClickOnGoing();
            }
        });

        return view;
    }

    private void initView(View view) {
        cardView = (RelativeLayout) view.findViewById(R.id.cardView);
        updateImg = (ImageView) view.findViewById(R.id.updateImg);
        layout = (LinearLayout) view.findViewById(R.id.layout);
        userMobileTxt = (TextView) view.findViewById(R.id.userMobile_txt);
        userEmailTxt = (TextView) view.findViewById(R.id.userEmail_txt);
        privacyTxt = (TextView) view.findViewById(R.id.privacy_txt);
        helpTxt = (TextView) view.findViewById(R.id.help_txt);
        userImageView = (CircularImageView) view.findViewById(R.id.userImageView);
        userNameTxt = (TextView) view.findViewById(R.id.userName_txt);
        useransTxt = (TextView) view.findViewById(R.id.userans_txt);
        totalEraing = (TextView) view.findViewById(R.id.totalEraing);
        totaltimetxt = (TextView) view.findViewById(R.id.totaltimetxt);
        tutorrating = (RatingBar) view.findViewById(R.id.tutorrating);
        ongoingLayout = (LinearLayout)view.findViewById(R.id.ongoingLayout);
        addPaymentInfotxt = (TextView) view.findViewById(R.id.addPaymentInfotxt);
        addPaymentInfotxt.setPaintFlags(addPaymentInfotxt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        getProfileDetail();

        if (isAnyChatLive){
            ongoingLayout.setVisibility(View.VISIBLE);
        }else{
            ongoingLayout.setVisibility(View.GONE);
        }

        privacyTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(getActivity(), TermsAndConditionsActivity.class);
                intent1.putExtra("_mode_specifier",TermsAndConditionsActivity.MODE_PRIVACY);
                startActivity(intent1);
            }
        });

        helpTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(getActivity(), TermsAndConditionsActivity.class);
                intent1.putExtra("_mode_specifier",TermsAndConditionsActivity.MODE_HELP);
                startActivity(intent1);
            }
        });
    }

    public void showOngoingLayout(boolean show){
        if(ongoingLayout!=null){
            if (show)ongoingLayout.setVisibility(View.VISIBLE);
            else ongoingLayout.setVisibility(View.GONE);
        }
    }

    private void getProfileDetail() {
        progressDialog.show();
        try {

            Call<ResponseBody> getUserResponse = apiService.getUserProfile(Georgeway_Application.getSharedPreferences().getString("userId", ""));

            getUserResponse.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (appCompatActivity==null || appCompatActivity.isFinishing()){
                        return;
                    }
                    progressDialog.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(new String(response.body().string()));
                        if (jsonObject.optString("status_code").equalsIgnoreCase("200")) {
                            JSONObject data = jsonObject.getJSONArray("data").getJSONObject(0);

                            userNameTxt.setText(data.optString("first_name") + " " + data.optString("last_name"));
                            if (!data.optString("about_us").equalsIgnoreCase("null")) {
                                userMobileTxt.setText(data.optString("about_us"));
                            }
                            if (!data.optString("image").isEmpty()) {
                                ImageLoader.getInstance().displayImage(data.optString("image"), userImageView, Georgeway_Application.intitOptions());
                            }
                            if (!data.optString("average_rating").equalsIgnoreCase("null")) {
                                tutorrating.setRating(Float.parseFloat(data.optString("average_rating")));
                            }
                            userEmailTxt.setText(data.optString("email"));

                            JSONArray jsonArray = data.optJSONArray("payment_history");
                            totalEraing.setText(jsonArray.getJSONObject(0).optString("amount") + "$");
                            if (jsonArray.getJSONObject(0).optString("total_time").equalsIgnoreCase("0")) {
                                totaltimetxt.setText("0H:0M:0S");
                            } else {
                                showWorkedHours((String)jsonArray.getJSONObject(0).optString("total_time"));
                            }


                        } else {
                            AppUtils.mShowDialog(appCompatActivity, jsonObject.optString("error_message"));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    progressDialog.dismiss();
                }
            });

        } catch (Exception e) {
            Log.e("success", "" + e.toString());
            e.printStackTrace();
            progressDialog.dismiss();

        }
    }

    private void showWorkedHours(String time) {
        long secsTotal = Long.valueOf(time);

        long hours = secsTotal / 3600;
        long temp = secsTotal % 3600;
        long minutes = temp / 60;
        long sec = temp % 60;

        totaltimetxt.setText(twoDigitString(hours) + "hr:" + twoDigitString(minutes) + "min:" + twoDigitString(sec) + "secs");
    }

    public static String twoDigitString(long number) {
        if (number>-1){
            if (number<10){
                return "0" + number;
            }else{
                return String.valueOf(number);
            }
        }else{
            return "00";
        }
    }

    private void forgotPasswordDialog() {
        View view = LayoutInflater.from(appCompatActivity).inflate(R.layout.add_paypal_dialog, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(appCompatActivity);
        builder.setView(view);
        Button submitBtn = (Button) view.findViewById(R.id.submitBtn);
        Button cancelBtn = (Button) view.findViewById(R.id.cancelBtn);
        final EditText userEmailEdittext = (EditText)view.findViewById(R.id.userEmail_edittext);



        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!userEmailEdittext.getText().toString().isEmpty())
                {
                    if (AppUtils.isValidEmailAddress(userEmailEdittext,appCompatActivity))
                    {
                        userForgotPassword(userEmailEdittext.getText().toString());
                        alertDialog.dismiss();
                    }else
                    {
                        AppUtils.mShowDialog(appCompatActivity,"Please enter valid email id");
                    }

                }else
                {
                    AppUtils.mShowDialog(appCompatActivity,"Please enter your email id");
                }
            }
        });
    }

    private void userForgotPassword(String email) {
        progressDialog.show();
        try {
            Call<ResponseBody> getUserResponse = apiService.addPaypayID(email,Georgeway_Application.getSharedPreferences().getString("userId", ""));

            getUserResponse.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    progressDialog.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(new String(response.body().string()));
                        if (jsonObject.optString("status_code").equalsIgnoreCase("200")) {
                            AppUtils.mShowDialog(appCompatActivity, jsonObject.optString("success_message"));
                        } else {
                            AppUtils.mShowDialog(appCompatActivity, jsonObject.optString("error_message"));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    progressDialog.dismiss();
                }
            });

        } catch (Exception e) {
            Log.e("success", "" + e.toString());
            e.printStackTrace();
            progressDialog.dismiss();

        }
    }
}
