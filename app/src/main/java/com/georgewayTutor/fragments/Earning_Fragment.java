package com.georgewayTutor.fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.georgewayTutor.Georgeway_Application;
import com.georgewayTutor.Models.s.history.PaymentHistoryResponse;
import com.georgewayTutor.R;
import com.georgewayTutor.adapters.Eraning_Adapter;
import com.georgewayTutor.api.APIService;
import com.georgewayTutor.api.GeorgewayApi;
import com.georgewayTutor.utils.AppUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by JAI on 1/7/2018.
 */

@SuppressLint("ValidFragment")
public class Earning_Fragment extends Fragment {
    private AppCompatActivity appCompatActivity;
    private ProgressDialog progressDialog;
    private GeorgewayApi apiService;
    private ListView listView;
    private TextView no_data_txt;

    public Earning_Fragment() {}

    public Earning_Fragment(AppCompatActivity appCompatActivity) {
        this.appCompatActivity = appCompatActivity;
        apiService = APIService.createService(GeorgewayApi.class);
        progressDialog = AppUtils.getProgressDialog(appCompatActivity);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.earning_fragment, container, false);
        listView = (ListView) view.findViewById(R.id.earningListView);
        no_data_txt = (TextView)view.findViewById(R.id.no_data_txt);
        getPaymentHistory();
        return view;
    }

    private void getPaymentHistory() {
        progressDialog.show();
        Call<PaymentHistoryResponse> getSchoolResponse = apiService.getPaymentHistory(Georgeway_Application.getSharedPreferences().getString("userId",""));
        getSchoolResponse.enqueue(new Callback<PaymentHistoryResponse>() {
            @Override
            public void onResponse(Call<PaymentHistoryResponse> call, Response<PaymentHistoryResponse> response) {

                try {
                    progressDialog.dismiss();
                    if (response.body().getStatus().equalsIgnoreCase("Ok"))
                    {
                        if (response.body().getData().size()>0)
                        {
                            listView.setVisibility(View.VISIBLE);
                            no_data_txt.setVisibility(View.GONE);
                            listView.setAdapter(new Eraning_Adapter(appCompatActivity,response.body().getData()));
                        }else
                        {
                            listView.setVisibility(View.GONE);
                            no_data_txt.setVisibility(View.VISIBLE);
                        }
                    }

                } catch (Exception e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<PaymentHistoryResponse> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }
}
