package com.georgewayTutor.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.georgewayTutor.Georgeway_Application;
import com.georgewayTutor.R;
import com.georgewayTutor.adapters.Home_Adapter;
import com.georgewayTutor.utils.AppUtils;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by JAI on 1/7/2018.
 */

@SuppressLint("ValidFragment")
public class Home_Fragment extends Fragment
{
    private AppCompatActivity appCompatActivity;
    private DatabaseReference databaseReference;
    private ArrayList<JSONObject> jsonObjects;
    private Home_Adapter home_adapter;
    public Home_Fragment (AppCompatActivity appCompatActivity)
    {
        this.appCompatActivity = appCompatActivity;
        databaseReference = FirebaseDatabase.getInstance().getReference("tutors/"+ Georgeway_Application.getSharedPreferences().getString("userId","")).child("chat_rooms");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.earning_fragment,container,false);
        ListView listView = (ListView)view.findViewById(R.id.earningListView);

        jsonObjects = new ArrayList<>();
        home_adapter = new Home_Adapter(appCompatActivity,jsonObjects);
        listView.setAdapter(home_adapter);
    //    databaseReference.addChildEventListener(childEventListener);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        jsonObjects.clear();
        databaseReference.addChildEventListener(childEventListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        databaseReference.removeEventListener(childEventListener);
    }

    private ChildEventListener childEventListener = new ChildEventListener() {
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
            if (dataSnapshot!=null)
            {
                try {
                    HashMap hashMap = (HashMap)dataSnapshot.getValue();
                    hashMap.put("key",dataSnapshot.getKey());
                    home_adapter.updateList(new JSONObject(hashMap));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

        }

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {

        }

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };
}
