package com.georgewayTutor;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.georgewayTutor.Models.s.UserIfoModel;
import com.georgewayTutor.api.APIService;
import com.georgewayTutor.api.GeorgewayApi;
import com.georgewayTutor.calling.BaseActivity;
import com.georgewayTutor.calling.SinchService;
import com.georgewayTutor.utils.AppUtils;
import com.georgewayTutor.utils.ViewAnimUtils;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInStatusCodes;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.sinch.android.rtc.SinchError;
import com.sinch.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by JAI on 1/13/2018.
 */

public class Login_Activity extends BaseActivity implements SinchService.StartFailedListener{
    private Toolbar toolbar;
    private EditText userEmailEdittext;
    private EditText userpasswordEdittext;
    private Button loginBtn;
    private TextView georgewayTxt;
    private GeorgewayApi apiService;
    private ProgressDialog progressDialog;
    CallbackManager callbackManager;
    private String first_name = "", last_name = "", facebokEmail = "", mfacebookId = "", mGender = "", mDob;
    private LoginButton loginButton;
    private ImageView facebook_img,google_img;
    private static final String TAG = "SignInActivity";
    private static final int RC_SIGN_IN = 9001;

    private GoogleSignInClient mGoogleSignInClient;
    private DatabaseReference databaseReference;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(getApplicationContext());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        databaseReference = FirebaseDatabase.getInstance().getReference("tutors");
        apiService = APIService.createService(GeorgewayApi.class);
        progressDialog = AppUtils.getProgressDialog(Login_Activity.this);
        initView();

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!userEmailEdittext.getText().toString().isEmpty()) {
                    if (!userpasswordEdittext.getText().toString().isEmpty()) {
                        mUserLogin();
                    } else {
                        AppUtils.mShowDialog(Login_Activity.this, "Please enter your password");
                    }
                } else {
                    AppUtils.mShowDialog(Login_Activity.this, "Please enter your email id");
                }
            }
        });

        georgewayTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Login_Activity.this, Register_Activity.class));
                ViewAnimUtils.activityEnterTransitions(Login_Activity.this);
               // finish();
            }
        });
        facebook_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginButton.performClick();
            }
        });

        google_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        });

        loginButton.setReadPermissions(Arrays.asList("public_profile", "email"));
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.e("login", " " + loginResult);

                if (AccessToken.getCurrentAccessToken() != null) {
                    RequestData();
                    Profile profile = Profile.getCurrentProfile();
                    if (profile != null) {
                        Log.e("facebook_id", profile.getId());
                        Log.e("f_name", profile.getFirstName());
                        Log.e("l_name", profile.getLastName());
                        Log.e("full_name", profile.getName());
                        first_name = profile.getFirstName();
                        last_name = profile.getLastName();
                        mfacebookId = profile.getId();

                    }
                }
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {
                Log.e("facebbok"," "+error.toString());
            }
        });

    }

    @Override
    protected void onServiceConnected() {
            getSinchServiceInterface().setStartListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onStartFailed(SinchError error) {
        Toast.makeText(this, error.toString(), Toast.LENGTH_LONG).show();
    }
    @Override
    public void onStarted()
    {
        //Toast.makeText(this,"Started Called",Toast.LENGTH_LONG).show();
    }

    // [START signOut]
    private void signOut() {
        mGoogleSignInClient.signOut().addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        // [START_EXCLUDE]
                        // [END_EXCLUDE]
                    }
                });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }else
        {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }

    }
    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            facebokEmail = account.getEmail();
            mGender = "";
            String fullname = account.getDisplayName();

            String[] parts = fullname.split(" ");
            Log.d("Length-->",""+parts.length);
            if(parts.length==2) {
                first_name = parts[0];
                last_name = parts[1];
                Log.d("First-->", "" + first_name);
                Log.d("Last-->", "" + last_name);
            }
            else if(parts.length==3){
                first_name = parts[0];
                String middlename = parts[1];
                last_name = parts[2];
            }
            if (AppUtils.isNetworkAvailable(Login_Activity.this)) {
                mFacebooklogin(account.getId(),"google",facebokEmail);
            } else {
                Snackbar.make(loginBtn, "No, Internet Connection, Please try again.", Snackbar.LENGTH_INDEFINITE).setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                }).show();
            }
        } catch (ApiException e) {
            String error = GoogleSignInStatusCodes.getStatusCodeString(e.getStatusCode());

            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
        }
    }
    public void RequestData() {
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {

                JSONObject json = response.getJSONObject();
                System.out.println("Json data :" + json);
                try {
                    if (json != null) {
                        Log.e("data ", " " + json);
                        facebokEmail = json.optString("email");
                        mGender = json.optString("gender");
                        if (AppUtils.isNetworkAvailable(Login_Activity.this)) {
                            mFacebooklogin(json.getString("id"),"fb",facebokEmail);
                        } else {
                            Snackbar.make(loginBtn, "No, Internet Connection, Please try again.", Snackbar.LENGTH_INDEFINITE).setAction("RETRY", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                }
                            }).show();
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,birthday,gender,email,picture");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void mFacebooklogin(final String id, final String fb, final String facebokEmail)
    {
        progressDialog.show();
        LoginManager.getInstance().logOut();
        signOut();
        try {

            Call<ResponseBody> getUserResponse = apiService.socialLogin(Georgeway_Application.getSharedPreferences().getString(AppUtils.FCM_TOKEN,""), Build.BRAND,"Android",fb,facebokEmail,id);

            getUserResponse.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    progressDialog.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(new String(response.body().string()));
                        if (jsonObject.optString("status_code").equalsIgnoreCase("200")) {
                            JSONObject data = jsonObject.getJSONArray("data").getJSONObject(0);
                            UserIfoModel userIfoModel = new UserIfoModel((HashMap) AppUtils.jsonToMap(data),"false",0);

                            Map<String, Object> stringObjectMap = new Gson().fromJson(data.toString(),new TypeToken<HashMap<String,Object>>() {}.getType());
                            databaseReference.child(data.optString("id")+"/details/").setValue(stringObjectMap);
                            databaseReference.child(data.optString("id")+"/is_online/").setValue(userIfoModel.getIs_online());
                            databaseReference.child(data.optString("id")+"/total_request_count/").setValue(userIfoModel.getTotal_request_count());

                                getSinchServiceInterface().startClient("T_"+data.optString("id"));

                            Georgeway_Application.getSharedPreferences().edit().putString("userId",data.optString("id")).putString("firstName",data.optString("first_name")).putString("lastName",data.optString("last_name")).putString("image",data.optString("image")).putString("isLogin","true").commit();
                            startActivity(new Intent(Login_Activity.this,MainActivity.class));
                            ViewAnimUtils.activityEnterTransitions(Login_Activity.this);
                            finishAffinity();
                        } else if (jsonObject.optString("status_code").equalsIgnoreCase("204")) {
                            mShowDialog(Login_Activity.this, jsonObject.optString("error_message"),fb,id);
                        }else
                        {

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    progressDialog.dismiss();
                }
            });

        } catch (Exception e) {
            Log.e("success", "" + e.toString());
            e.printStackTrace();
            progressDialog.dismiss();

        }
    }
    public  void mShowDialog(AppCompatActivity appCompatActivity, String msg, final String fb, final String id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(appCompatActivity);
        builder.setIcon(R.drawable.ic_launcher);

        builder.setTitle(appCompatActivity.getResources().getString(R.string.app_name));
        builder.setMessage(msg);


        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                startActivity(new Intent(Login_Activity.this,Register_Activity.class).putExtra("firstName",first_name).putExtra("lastName",last_name).putExtra("email",facebokEmail).putExtra("id",id).putExtra("type",fb));
            }
        });
        AlertDialog alertDialog = builder.create();
        int textColorId = appCompatActivity.getResources().getIdentifier("alertMessage", "id", "android");
        TextView textColor = (TextView) alertDialog.findViewById(textColorId);
        if (textColor != null) {
            textColor.setTextColor(Color.BLACK);
        }
        alertDialog.show();
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        userEmailEdittext = (EditText) findViewById(R.id.userEmail_edittext);
        userpasswordEdittext = (EditText) findViewById(R.id.userpassword_edittext);
        loginBtn = (Button) findViewById(R.id.loginBtn);
        georgewayTxt = (TextView) findViewById(R.id.georgewayTxt);
        callbackManager = CallbackManager.Factory.create();
        loginButton = (LoginButton) findViewById(R.id.login_button);
        google_img   = (ImageView)findViewById(R.id.google_img);
        facebook_img = (ImageView)findViewById(R.id.facebook_img);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        // [END configure_signin]

        // [START build_client]
        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        // [END build_client]
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ViewAnimUtils.activityExitTransitions(Login_Activity.this);
        finish();
    }

    private void mUserLogin() {
        progressDialog.show();
        try {

            final String token = Georgeway_Application.getSharedPreferences().getString(AppUtils.FCM_TOKEN,"");
            if (token.isEmpty()){
                Toast.makeText(this, "Something went wrong, Please restart the app or try again after a while", Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
                return;
            }
            Call<ResponseBody> getUserResponse = apiService.userLogin(token,
                    userEmailEdittext.getText().toString(),
                    userpasswordEdittext.getText().toString(),
                    Build.DEVICE,
                    "android");

            AppUtils.hideSoftKeyboard(this);

            getUserResponse.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    progressDialog.dismiss();
                    try {
                        ResponseBody rb = response.body();
                        if (rb==null){
                            return;
                        }
                        String jsonStr = rb.string();
                        JSONObject jsonObject = new JSONObject(jsonStr);
                        if (jsonObject.optString("status_code").equalsIgnoreCase("200")) {
                            JSONObject data = jsonObject.getJSONArray("data").getJSONObject(0);
                            UserIfoModel userIfoModel = new UserIfoModel((HashMap) AppUtils.jsonToMap(data),"false",0);
                            Map<String, Object> stringObjectMap = new Gson().fromJson(data.toString(),new TypeToken<HashMap<String,Object>>() {}.getType());
                            databaseReference.child(data.optString("id")+"/details/").setValue(stringObjectMap);
                            databaseReference.child(data.optString("id")+"/is_online/").setValue(userIfoModel.getIs_online());
                            databaseReference.child(data.optString("id")+"/total_request_count/").setValue(userIfoModel.getTotal_request_count());

                            Log.e("TAG","LOGIN SUCCESS: \n" + userIfoModel.getdetails().toString());

                            getSinchServiceInterface().startClient("T_"+data.optString("id"));

                            Georgeway_Application.getSharedPreferences().edit().
                                    putString("userId",data.optString("id")).
                                    putString("firstName",data.optString("first_name")).
                                    putString("lastName",data.optString("last_name")).
                                    putString("image",data.optString("image")).
                                    putString("isLogin","true").apply();
                            startActivity(new Intent(Login_Activity.this,MainActivity.class));
                            ViewAnimUtils.activityEnterTransitions(Login_Activity.this);
                            finishAffinity();
                        } else {
                            AppUtils.mShowDialog(Login_Activity.this, jsonObject.optString("error_message"));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    progressDialog.dismiss();
                }
            });

        } catch (Exception e) {
            Log.e("success", "" + e.toString());
            e.printStackTrace();
            progressDialog.dismiss();

        }
    }

    private void forgotPasswordDialog() {
        View view = LayoutInflater.from(Login_Activity.this).inflate(R.layout.forgot_password_dialog, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(Login_Activity.this);
        builder.setView(view);
        Button submitBtn = (Button) view.findViewById(R.id.submitBtn);
        Button cancelBtn = (Button) view.findViewById(R.id.cancelBtn);
        final EditText userEmailEdittext = (EditText)view.findViewById(R.id.userEmail_edittext);



        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!userEmailEdittext.getText().toString().isEmpty())
                {
                    if (AppUtils.isValidEmailAddress(userEmailEdittext,Login_Activity.this))
                    {
                        userForgotPassword(userEmailEdittext.getText().toString());
                        alertDialog.dismiss();
                    }else
                    {
                        AppUtils.mShowDialog(Login_Activity.this,"Please enter valid email id");
                    }

                }else
                {
                    AppUtils.mShowDialog(Login_Activity.this,"Please enter your email id");
                }
            }
        });
    }

    private void userForgotPassword(String email)
    {
        progressDialog.show();
        try {

            Call<ResponseBody> getUserResponse = apiService.userForgotpassword(email);

            getUserResponse.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    progressDialog.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(new String(response.body().string()));
                        if (jsonObject.optString("status_code").equalsIgnoreCase("200")) {
                            AppUtils.mShowDialog(Login_Activity.this, jsonObject.optString("success_message"));
                        } else {
                            AppUtils.mShowDialog(Login_Activity.this, jsonObject.optString("error_message"));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    progressDialog.dismiss();
                }
            });

        } catch (Exception e) {
            Log.e("success", "" + e.toString());
            e.printStackTrace();
            progressDialog.dismiss();

        }
    }

    public void forgotPassword(View view)
    {
        forgotPasswordDialog();
    }
}
