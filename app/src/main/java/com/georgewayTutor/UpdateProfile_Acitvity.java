package com.georgewayTutor;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.abdeveloper.library.MultiSelectDialog;
import com.abdeveloper.library.MultiSelectModel;
import com.georgewayTutor.Models.s.UserIfoModel;
import com.georgewayTutor.api.APIService;
import com.georgewayTutor.api.GeorgewayApi;
import com.georgewayTutor.utils.AppUtils;
import com.georgewayTutor.utils.CircularImageView;
import com.georgewayTutor.utils.ViewAnimUtils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by JAI on 1/20/2018.
 */

public class UpdateProfile_Acitvity extends AppCompatActivity {
    private Toolbar toolbar;
    private TextView titleTxt;
    private TextView updateTxt;
    private RatingBar userRating;
    private TextView updatePassword;
    private CircularImageView userImageView;
    private EditText userNameTxt, userInfo_edittext;
    private EditText userfirstEdittext;
    private EditText userlastEdittext;
    private EditText userEmailEdittext;
    private EditText userschoolEdittext;
    private EditText usercourseEdittext, userage_edittext,firstName_edit, lastName_edit;
    private ProgressDialog progressDialog;
    private GeorgewayApi apiService;
    private RadioGroup segmentedControl;
    private RadioButton opt1;
    private RadioButton opt2;
    private ImageView backbtn;
    private ArrayList<String> schoolNameList, schoolIdList, courseNameList, courseIdList, selectCourse;
    private JSONArray course_ids = new JSONArray();
    private String schoolIds = "";
    private BottomSheetDialog chooseMediaMenu;
    private LinearLayout cameraLayout;
    private LinearLayout galleryLayout;
    private TextView cancelTxt;
    private Uri mImageCaptureUri;
    private File imageFile;
    private String gender = "";
    private ImageView profile_btn,about_btn;
    private ArrayList<String> usercourse_id = new ArrayList<>();
    private ArrayList<String> usercourse_name = new ArrayList<>();
    boolean[] is_checked;
    private DatabaseReference databaseReference,userRequestCounter;
    private int count_request;
    ArrayList<MultiSelectModel> schoolLists,courseLists;
    private MultiSelectDialog multiSelectDialog,cousreMultiselectDilaog;
    private ArrayList<Integer> alreadySelectedSchool,alreadySelectedCourse;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.account_update_actiivty);
        databaseReference = FirebaseDatabase.getInstance().getReference("tutors");
        userRequestCounter = FirebaseDatabase.getInstance().getReference("tutors").child(Georgeway_Application.getSharedPreferences().getString("userId", "")).child("total_request_count");
        apiService = APIService.createService(GeorgewayApi.class);
        progressDialog = AppUtils.getProgressDialog(UpdateProfile_Acitvity.this);
        initView();

        getProfileDetail();
        getSchoolList();
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewAnimUtils.activityExitTransitions(UpdateProfile_Acitvity.this);
                finish();
            }
        });


        userRequestCounter.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot!=null)
                {
                    count_request = Integer.parseInt(""+dataSnapshot.getValue());

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        userschoolEdittext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                final CharSequence[] dialogList = schoolNameList.toArray(new CharSequence[schoolNameList.size()]);
//                AlertDialog.Builder builder = new AlertDialog.Builder(UpdateProfile_Acitvity.this);
//                builder.setTitle("Select school where can teach")
//                        .setItems(dialogList, new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int item) {
//                                Log.i("Dialogos", "Opción elegida: " + dialogList[item]);
//                                userschoolEdittext.setText("" + dialogList[item]);
//                                schoolIds = schoolIdList.get(item);
//                            }
//                        });
//
//                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//                        dialogInterface.dismiss();
//                    }
//                });
//                AlertDialog alertDialog = builder.create();
//                alertDialog.show();

                multiSelectDialog = new MultiSelectDialog()
                        .title("Select school where can teach") //setting title for dialog
                        .titleSize(15)
                        .positiveText("Done")
                        .negativeText("Cancel")
                        .setMinSelectionLimit(1)
                        .setMaxSelectionLimit(1)
                        .preSelectIDsList(alreadySelectedSchool) //List of ids that you need to be selected
                        .multiSelectList(schoolLists) // the multi select model list with ids and name
                        .onSubmit(new MultiSelectDialog.SubmitCallbackListener() {
                            @Override
                            public void onSelected(ArrayList<Integer> selectedIds, ArrayList<String> selectedNames, String dataString) {
                                //will return list of selected IDS
                                for (int i = 0; i < selectedIds.size(); i++) {
                                    userschoolEdittext.setText("" + selectedNames.get(i));
                                    schoolIds = ""+selectedIds.get(i);
                                }


                            }

                            @Override
                            public void onCancel() {
                                Log.d("","Dialog cancelled");

                            }
                        });

                multiSelectDialog.show(getSupportFragmentManager(), "multiSelectDialog");

            }
        });

        usercourseEdittext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                final CharSequence[] dialogList = courseNameList.toArray(new CharSequence[courseNameList.size()]);
//                AlertDialog.Builder builder = new AlertDialog.Builder(UpdateProfile_Acitvity.this);
//                builder.setTitle("Select course you can teach");
//                builder.setMultiChoiceItems(dialogList, is_checked,
//                        new DialogInterface.OnMultiChoiceClickListener() {
//                            @SuppressLint("NewApi")
//                            public void onClick(DialogInterface dialog, int whichButton, boolean isChecked) {
//                                Log.e("course", " " + dialogList[whichButton]);
//
//                                if (isChecked) {
//                                    selectCourse.add("" + dialogList[whichButton]);
//                                    course_ids.put(courseIdList.get(whichButton));
//                                    usercourse_id.add(""+courseIdList.get(whichButton));
//                                } else {
//                                    selectCourse.remove("" + dialogList[whichButton]);
//                                    course_ids.remove(whichButton);
//                                    usercourse_id.remove(""+courseIdList.get(whichButton));
//                                }
//                            }
//                        });
//
//                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//                        dialogInterface.dismiss();
//                        String name = TextUtils.join(",", selectCourse);
//                        usercourseEdittext.setText(name);
//                        Log.e("Register", " " + course_ids);
//
//                    }
//                });
//                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//                        dialogInterface.dismiss();
//                    }
//                });
//                AlertDialog alertDialog = builder.create();
//                alertDialog.show();


                cousreMultiselectDilaog = new MultiSelectDialog()
                        .title("Select course you can teach") //setting title for dialog
                        .titleSize(15)
                        .positiveText("Done")
                        .negativeText("Cancel")
                        .setMinSelectionLimit(1)
                        .setMaxSelectionLimit((courseLists!=null) ? courseLists.size() : 1)
                        .preSelectIDsList(alreadySelectedCourse) //List of ids that you need to be selected
                        .multiSelectList(courseLists) // the multi select model list with ids and name
                        .onSubmit(new MultiSelectDialog.SubmitCallbackListener() {
                            @Override
                            public void onSelected(ArrayList<Integer> selectedIds, ArrayList<String> selectedNames, String dataString) {
                                //will return list of selected IDS
                                usercourse_id.clear();
                                for (int i = 0; i < selectedIds.size(); i++) {
                                    usercourse_id.add(""+selectedIds.get(i));
                                }
                                String name = TextUtils.join(",", selectedNames);
                                usercourseEdittext.setText(name);

                            }

                            @Override
                            public void onCancel() {
                                Log.d("","Dialog cancelled");

                            }
                        });

                cousreMultiselectDilaog.show(getSupportFragmentManager(), "multiSelectDialog");
            }
        });

        chooseMediaMenu = new BottomSheetDialog(UpdateProfile_Acitvity.this);
        chooseMediaMenu.setContentView(R.layout.media_choose_layout);
        cameraLayout = (LinearLayout) chooseMediaMenu.findViewById(R.id.camera_layout);
        galleryLayout = (LinearLayout) chooseMediaMenu.findViewById(R.id.gallery_layout);
        cancelTxt = (TextView) chooseMediaMenu.findViewById(R.id.cancel_txt);

        userImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (shouldAskPermissions()) {
                    askPermissions_1();
                } else {
                    if (chooseMediaMenu.isShowing()) {
                        chooseMediaMenu.dismiss();
                    } else {
                        chooseMediaMenu.show();
                    }
                }
            }
        });

        cameraLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOpenCamera();
                chooseMediaMenu.dismiss();
            }
        });
        galleryLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOpenGallery();
                chooseMediaMenu.dismiss();
            }
        });

        cancelTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseMediaMenu.dismiss();
            }
        });


        profile_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userNameTxt.setEnabled(true);
            }
        });
        about_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userInfo_edittext.setEnabled(true);
            }
        });

        updateTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mUpdateUserProfile();
            }
        });
    }

    protected boolean shouldAskPermissions() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }


    // Permissions variables
    private static final int REQUEST_CAMERA_PERMISSIO = 2;
    private static String[] PERMISSIONS_CAMERA = {
            android.Manifest.permission.CAMERA, android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE

    };


    @TargetApi(23)
    protected void askPermissions_1() {
        int cameraPermission = ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA);
        int writePermission = ActivityCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int readPermission = ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE);


        if (cameraPermission != PackageManager.PERMISSION_GRANTED || readPermission != PackageManager.PERMISSION_GRANTED || writePermission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(this, PERMISSIONS_CAMERA, REQUEST_CAMERA_PERMISSIO);
        } else {
            if (chooseMediaMenu.isShowing()) {
                chooseMediaMenu.dismiss();
            } else {
                chooseMediaMenu.show();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_CAMERA_PERMISSIO) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // save file
                if (chooseMediaMenu.isShowing()) {
                    chooseMediaMenu.dismiss();
                } else {
                    chooseMediaMenu.show();
                }
            } else {
                Toast.makeText(getApplicationContext(), "PERMISSION DENIED", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void mOpenCamera() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = new File(Environment.getExternalStorageDirectory(), "tmp_avatar_" + String.valueOf(System.currentTimeMillis()) + ".jpg");
        mImageCaptureUri = Uri.fromFile(file);

        try {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            intent.putExtra("return-data", true);

            startActivityForResult(intent, 1001);
            ViewAnimUtils.activityEnterTransitions(UpdateProfile_Acitvity.this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void mOpenGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(intent, 2);
        ViewAnimUtils.activityEnterTransitions(UpdateProfile_Acitvity.this);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1001) {
            try {


                String path = mImageCaptureUri.getPath();
                imageFile = new File(path);
                Bitmap thumbnail = (BitmapFactory.decodeFile(mImageCaptureUri.getPath()));

                Log.e("imageFile", " " + imageFile.getAbsolutePath());

//                profilePic.setImageBitmap(thumbnail);
                ImageLoader.getInstance().displayImage("file://" + imageFile, userImageView, Georgeway_Application.intitOptions());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == 2) {
            try {
                mImageCaptureUri = data.getData();
                String path = getPath(getApplicationContext(), mImageCaptureUri);

                Bitmap thumbnail = (BitmapFactory.decodeFile(path));


                Log.e("path  from", path + "");

                imageFile = new File(path);
//                profilePic.setImageBitmap(thumbnail);
                ImageLoader.getInstance().displayImage("file://" + imageFile, userImageView, Georgeway_Application.intitOptions());
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static String getPath(final Context context, final Uri uri) {
        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            } else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{split[1]};
                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        } else if ("content".equalsIgnoreCase(uri.getScheme())) {
            if (isGooglePhotosUri(uri)) return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};
        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null) cursor.close();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    private void mUpdateUserProfile() {
        progressDialog.show();
        RequestBody user_id = null;
        RequestBody first_name = null;
        RequestBody last_name = null;
        RequestBody emailRes = null;
        RequestBody age = null;
        RequestBody gender1 = null;
        RequestBody school_id = null;
        RequestBody cousre_id = null;
        RequestBody about_us = null;
        MultipartBody.Part body = null;
        try {
            int selectedId = segmentedControl.getCheckedRadioButtonId();
            opt1 = (RadioButton) findViewById(selectedId);
            if (opt1.getText().toString().equalsIgnoreCase("Male")) {
                gender = "1";
            } else {
                gender = "2";
            }
            user_id = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(Georgeway_Application.getSharedPreferences().getString("userId", "")));
            first_name = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(firstName_edit.getText().toString()));
            last_name = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(lastName_edit.getText().toString()));

            emailRes = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(userEmailEdittext.getText().toString()));
            age = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(userage_edittext.getText().toString()));

            gender1 = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(gender));
            school_id = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(schoolIds));
            cousre_id = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(TextUtils.join(",",usercourse_id)));
            about_us = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(userInfo_edittext.getText().toString()));
            body = null;
            if (imageFile != null) {
                RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), imageFile);
                body = MultipartBody.Part.createFormData("image", imageFile.getName(), reqFile);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        Call<ResponseBody> req = apiService.userDetailUpdate(first_name, last_name, emailRes, age, gender1, school_id, cousre_id, user_id, about_us, body);
        req.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    progressDialog.dismiss();
                    JSONObject responseObject = new JSONObject(response.body().string());
                    updateProfileFirebase();
                    if (responseObject.optString("status_code").equalsIgnoreCase("200")) {
                        // AppUtils.mShowDialog(UpdateProfile_Acitvity.this, responseObject.optString("message"));
                        getProfileDetail();
                    } else {
                        AppUtils.mShowDialog(UpdateProfile_Acitvity.this, responseObject.optString("message"));
                    }

//
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                progressDialog.dismiss();
            }
        });
    }

    private void getProfileDetail() {
        progressDialog.show();
        try {

            Call<ResponseBody> getUserResponse = apiService.getUserProfile(Georgeway_Application.getSharedPreferences().getString("userId", ""));

            getUserResponse.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    progressDialog.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(new String(response.body().string()));
                        if (jsonObject.optString("status_code").equalsIgnoreCase("200"))
                        {
                            JSONObject data = jsonObject.getJSONArray("data").getJSONObject(0);

                            UserIfoModel userIfoModel = new UserIfoModel((HashMap) AppUtils.jsonToMap(data),"true",(int)count_request);
                            Map<String, Object> stringObjectMap = new Gson().fromJson(data.toString(),new TypeToken<HashMap<String,Object>>() {}.getType());
                            databaseReference.child(data.optString("id")).child("details").setValue(stringObjectMap);
                            Georgeway_Application.getSharedPreferences().edit().putString("userId",data.optString("id")).putString("firstName",data.optString("first_name")).putString("lastName",data.optString("last_name")).putString("image",data.optString("image")).putString("isLogin","true").commit();

                            userEmailEdittext.setText(data.optString("email"));
                            userage_edittext.setText(data.optString("age"));
                            userNameTxt.setText(data.optString("first_name") + " " + data.optString("last_name"));
                            lastName_edit.setText(data.optString("last_name"));
                            firstName_edit.setText(data.optString("first_name"));

                            if (!data.optString("about_us").equalsIgnoreCase("null")) {
                                userInfo_edittext.setText(data.optString("about_us"));
                            }
                            if (data.optString("gender").equalsIgnoreCase("1")) {
                                opt1.setChecked(true);
                                opt2.setChecked(false);

                            } else {
                                opt2.setChecked(true);
                                opt1.setChecked(false);
                            }
                            if (data.optString("social_type").isEmpty()) {
                                updatePassword.setVisibility(View.VISIBLE);
                            }
                            alreadySelectedSchool = new ArrayList<>();
                            alreadySelectedCourse = new ArrayList<>();
                            alreadySelectedSchool.add(Integer.parseInt(data.optString("school_id")));
                            schoolIds = data.optString("school_id");
                            userschoolEdittext.setText(data.optString("school_name"));
                            if (!data.optString("image").isEmpty()) {
                                ImageLoader.getInstance().displayImage(data.optString("image"), userImageView, Georgeway_Application.intitOptions());
                            }
                            selectCourse = new ArrayList<>();
                            usercourse_id.clear();
                            for (int i = 0; i < data.optJSONArray("tutor_courses").length(); i++)
                            {
                                alreadySelectedCourse.add(Integer.parseInt(data.optJSONArray("tutor_courses").optJSONObject(i).optString("id")));

                                usercourse_id.add(data.optJSONArray("tutor_courses").optJSONObject(i).optString("id"));
                                selectCourse.add(data.optJSONArray("tutor_courses").optJSONObject(i).optString("name"));
                            }

                            usercourseEdittext.setText(""+TextUtils.join(",",selectCourse));

                        } else {
                            AppUtils.mShowDialog(UpdateProfile_Acitvity.this, jsonObject.optString("error_message"));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    progressDialog.dismiss();
                }
            });

        } catch (Exception e) {
            Log.e("success", "" + e.toString());
            e.printStackTrace();
            progressDialog.dismiss();

        }

    }

    private void updateProfileFirebase() {
        try {

//            webview.loadUrl("http://georgewayglobal.com/tutor/add_tutor_in_firebase.php?id="+Integer.parseInt(Georgeway_Application.getSharedPreferences().getString("userId", "")));
//            Call<ResponseBody> getUserResponse = apiService.updateFirebaseUserProfile("http://georgewayglobal.com/tutor/add_tutor_in_firebase.php?id="+Georgeway_Application.getSharedPreferences().getString("userId", ""));
//
//            getUserResponse.enqueue(new Callback<ResponseBody>() {
//                @Override
//                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                    try {
//                        JSONObject jsonObject = new JSONObject(response.body().string());
//                        Log.e("jsonObject", "" + jsonObject);
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<ResponseBody> call, Throwable t) {
//                }
//            });
//
        } catch (Exception e) {
            Log.e("success", "" + e.toString());
            e.printStackTrace();

        }

    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        profile_btn = (ImageView)findViewById(R.id.profile_btn);
        about_btn   = (ImageView)findViewById(R.id.about_btn);
        titleTxt = (TextView) findViewById(R.id.title_txt);
        updateTxt = (TextView) findViewById(R.id.update_txt);
        userRating = (RatingBar) findViewById(R.id.userRating);
        updatePassword = (TextView) findViewById(R.id.updatePassword);
        userImageView = (CircularImageView) findViewById(R.id.userImageView);
        userNameTxt = (EditText) findViewById(R.id.userName_txt);
        userInfo_edittext = (EditText) findViewById(R.id.userInfo_edittext);
        userfirstEdittext = (EditText) findViewById(R.id.userfirst_edittext);
        userlastEdittext = (EditText) findViewById(R.id.userlast_edittext);
        userEmailEdittext = (EditText) findViewById(R.id.userEmail_edittext);
        userschoolEdittext = (EditText) findViewById(R.id.userschool_edittext);
        usercourseEdittext = (EditText) findViewById(R.id.usercourse_edittext);
        userage_edittext = (EditText) findViewById(R.id.userage_edittext);
        firstName_edit = (EditText) findViewById(R.id.firstName_edit);
        lastName_edit = (EditText) findViewById(R.id.lastName_edit);
        segmentedControl = (RadioGroup) findViewById(R.id.segmented_control);
        opt1 = (RadioButton) findViewById(R.id.opt_1);
        opt2 = (RadioButton) findViewById(R.id.opt_2);
        backbtn = (ImageView) findViewById(R.id.backbtn);
    }

    private void getSchoolList() {
        progressDialog.show();
        Call<ResponseBody> getSchoolResponse = apiService.getSchoolList();
        getSchoolResponse.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    progressDialog.dismiss();
                    JSONObject jsonObject = new JSONObject(new String(response.body().string()));
                    if (jsonObject.optString("status_code").equalsIgnoreCase("200")) {
                        schoolIdList = new ArrayList<>();
                        schoolNameList = new ArrayList<>();
                        schoolLists= new ArrayList<>();
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            schoolNameList.add(jsonArray.optJSONObject(i).optString("name"));
                            schoolIdList.add(jsonArray.optJSONObject(i).optString("id"));
                            schoolLists.add(new MultiSelectModel(Integer.parseInt(jsonArray.optJSONObject(i).optString("id")),jsonArray.optJSONObject(i).optString("name")));
                          //  schoolItem.add(new SampleModel(jsonArray.optJSONObject(i).optString("name"),jsonArray.optJSONObject(i).optString("id")));
                        }
                    }
                    getCourseList();
                } catch (Exception e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                    getCourseList();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    private void getCourseList() {
        progressDialog.show();
        Call<ResponseBody> getSchoolResponse = apiService.getCourseList();
        getSchoolResponse.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    progressDialog.dismiss();
                    JSONObject jsonObject = new JSONObject(new String(response.body().string()));
                    if (jsonObject.optString("status_code").equalsIgnoreCase("200")) {
                        courseIdList = new ArrayList<>();
                        courseNameList = new ArrayList<>();
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        courseLists = new ArrayList<>();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            courseNameList.add(jsonArray.optJSONObject(i).optString("name"));
                            courseIdList.add(jsonArray.optJSONObject(i).optString("id"));
                            courseLists.add(new MultiSelectModel(Integer.parseInt(jsonArray.optJSONObject(i).optString("id")),jsonArray.optJSONObject(i).optString("name")));
                        }

                        is_checked = new boolean[courseNameList.size()];
                        for (int i = 0; i < courseIdList.size(); i++) {
                            for (int j = 0; j < usercourse_id.size(); j++) {
                                if (courseIdList.get(i).equalsIgnoreCase(usercourse_id.get(j)))
                                {
                                    is_checked[i]=true;
                                    break;
                                }else
                                {
                                    is_checked[i]=false;
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }
}
