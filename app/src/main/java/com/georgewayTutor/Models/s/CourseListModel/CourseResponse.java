package com.georgewayTutor.Models.s.CourseListModel;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class CourseResponse{

	@SerializedName("status_code")
	private String statusCode;

	@SerializedName("data")
	private List<DataItem> data;

	@SerializedName("success_message")
	private String successMessage;

	@SerializedName("status")
	private String status;

	public void setStatusCode(String statusCode){
		this.statusCode = statusCode;
	}

	public String getStatusCode(){
		return statusCode;
	}

	public void setData(List<DataItem> data){
		this.data = data;
	}

	public List<DataItem> getData(){
		return data;
	}

	public void setSuccessMessage(String successMessage){
		this.successMessage = successMessage;
	}

	public String getSuccessMessage(){
		return successMessage;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"CourseResponse{" + 
			"status_code = '" + statusCode + '\'' + 
			",data = '" + data + '\'' + 
			",success_message = '" + successMessage + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}