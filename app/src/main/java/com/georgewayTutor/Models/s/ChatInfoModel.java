package com.georgewayTutor.Models.s;

import java.util.Map;

/**
 * Created by JAI on 3/11/2018.
 */

public class ChatInfoModel
{
    public ChatModel getLast_msg() {
        return last_msg;
    }

    public void setLast_msg(ChatModel last_msg) {
        this.last_msg = last_msg;
    }

    private ChatModel last_msg;
    private String is_read;
    private String is_active;
    private String first_name;
    private String last_name;
    private String image;

    public String getIs_active() {
        return is_active;
    }

    public void setIs_active(String is_active) {
        this.is_active = is_active;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTutor_id() {
        return tutor_id;
    }

    public void setTutor_id(String tutor_id) {
        this.tutor_id = tutor_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    private String tutor_id;

    public ChatInfoModel(ChatModel last_msg, String is_read, Map<String, String> timestamp, String active_session, String first_name, String last_name, String image, String tutor_id, String user_id) {
        this.last_msg = last_msg;
        this.is_read = is_read;
        this.is_active = active_session;
        this.first_name = first_name;
        this.last_name = last_name;
        this.image = image;
        this.tutor_id = tutor_id;
        this.user_id = user_id;
        this.timestamp = timestamp;
    }

    private String user_id;

    public String getIs_read() {
        return is_read;
    }

    public void setIs_read(String is_read) {
        this.is_read = is_read;
    }

    public Map<String, String> getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Map<String, String> timestamp) {
        this.timestamp = timestamp;
    }

    private Map<String, String> timestamp;



}
