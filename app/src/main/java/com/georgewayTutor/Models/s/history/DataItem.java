package com.georgewayTutor.Models.s.history;

import com.google.gson.annotations.SerializedName;

public class DataItem{

	@SerializedName("tutor_id")
	private String tutorId;

	@SerializedName("minute_price")
	private String minutePrice;

	@SerializedName("amount")
	private String amount;

	@SerializedName("created")
	private String created;

	@SerializedName("tutor_paypal_email_id")
	private String tutorPaypalEmailId;

	@SerializedName("id")
	private String id;

	@SerializedName("total_time")
	private String totalTime;

	@SerializedName("tax_id")
	private String taxId;

	@SerializedName("status")
	private String status;

	public void setTutorId(String tutorId){
		this.tutorId = tutorId;
	}

	public String getTutorId(){
		return tutorId;
	}

	public void setMinutePrice(String minutePrice){
		this.minutePrice = minutePrice;
	}

	public String getMinutePrice(){
		return minutePrice;
	}

	public void setAmount(String amount){
		this.amount = amount;
	}

	public String getAmount(){
		return amount;
	}

	public void setCreated(String created){
		this.created = created;
	}

	public String getCreated(){
		return created;
	}

	public void setTutorPaypalEmailId(String tutorPaypalEmailId){
		this.tutorPaypalEmailId = tutorPaypalEmailId;
	}

	public String getTutorPaypalEmailId(){
		return tutorPaypalEmailId;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setTotalTime(String totalTime){
		this.totalTime = totalTime;
	}

	public String getTotalTime(){
		return totalTime;
	}

	public void setTaxId(String taxId){
		this.taxId = taxId;
	}

	public String getTaxId(){
		return taxId;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"tutor_id = '" + tutorId + '\'' + 
			",minute_price = '" + minutePrice + '\'' + 
			",amount = '" + amount + '\'' + 
			",created = '" + created + '\'' + 
			",tutor_paypal_email_id = '" + tutorPaypalEmailId + '\'' + 
			",id = '" + id + '\'' + 
			",total_time = '" + totalTime + '\'' + 
			",tax_id = '" + taxId + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}