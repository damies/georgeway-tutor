package com.georgewayTutor.Models.s;

import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by JAI on 2/18/2018.
 */

public class UserIfoModel
{
    private HashMap details;
    private String is_online;
    private int total_request_count;

    public UserIfoModel(HashMap details, String is_online, int total_request_count) {
        this.details = details;
        this.is_online = is_online;
        this.total_request_count = total_request_count;
    }

    public HashMap getdetails() {
        return details;
    }

    public void setdetails(HashMap details) {
        this.details = details;
    }


    public String getIs_online() {
        return is_online;
    }

    public void setIs_online(String is_online) {
        this.is_online = is_online;
    }

    public int getTotal_request_count() {
        return total_request_count;
    }

    public void setTotal_request_count(int total_request_count) {
        this.total_request_count = total_request_count;
    }

    @Override
    public String toString() {
        try {
            return Arrays.asList(details).toString();
        }catch (Exception e){
            return "";
        }
    }
}
