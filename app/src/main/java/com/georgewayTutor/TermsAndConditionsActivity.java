package com.georgewayTutor;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.georgewayTutor.api.APIService;
import com.georgewayTutor.utils.AppUtils;
import com.georgewayTutor.utils.ViewAnimUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class TermsAndConditionsActivity extends AppCompatActivity implements View.OnClickListener {


    ImageView imageViewBack;
    WebView webViewAboutUs;
    Toolbar app_bar;
    TextView textViewHeader;
    ProgressDialog mDialog;
    APIService apiService;

    public final static int MODE_PRIVACY=9;
    public final static int MODE_HELP=11;
    private AppCompatActivity mActivity;

    int WORKING_MODE=0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity_about_us);

        Bundle b = getIntent().getExtras();
        if (b!=null){
            WORKING_MODE = b.getInt("_mode_specifier");
        }
        /**
         * init
         */
        imageViewBack = findViewById(R.id.imageViewBack);
        webViewAboutUs = findViewById(R.id.webViewAboutUs);
        app_bar = findViewById(R.id.app_bar);
        textViewHeader = findViewById(R.id.textViewHeader);
        mDialog = AppUtils.getProgressDialog(TermsAndConditionsActivity.this);


        /**
         * init
         */
        init();

        /**
         * set on click events
         */
        setOnClickEvents();


        /**
         *get about us screen data
         */
       // getAboutUsRequest();

    }


    /**
     * setting up onclick on views
     */
    private void setOnClickEvents() {

        imageViewBack.setOnClickListener(this);

     }


    /* * initialize class
    */
    private void init() {

        mActivity = this;
        if (WORKING_MODE==MODE_PRIVACY) textViewHeader.setText(getResources().getString(R.string.Terms_and_Conditions));
        else if (WORKING_MODE==MODE_HELP) textViewHeader.setText(getString(R.string.contact_us));

        /**
         * Initializing objects from inflating from xml to perform actions
         */
        webViewAboutUs.getSettings().setJavaScriptEnabled(true);
        webViewAboutUs.getSettings().setBuiltInZoomControls(true);
        webViewAboutUs.getSettings().setUseWideViewPort(true);
        webViewAboutUs.getSettings().setLoadWithOverviewMode(true);
        if (WORKING_MODE == MODE_PRIVACY){
            webViewAboutUs.loadUrl(APIService.URL_TERMS_AND_CONDITIONS);
        }else if (WORKING_MODE == MODE_HELP){
            webViewAboutUs.loadUrl(APIService.URL_HELP);
        }

        webViewAboutUs.getSettings().setDisplayZoomControls(true);
        webViewAboutUs.getSettings().setBuiltInZoomControls(true);
        webViewAboutUs.setWebViewClient(new SSLTolerentWebViewClient());
        WebSettings webSettings = webViewAboutUs.getSettings();
        webSettings.setTextZoom(webSettings.getTextZoom() + 90);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        ViewAnimUtils.activityExitTransitions(mActivity);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.imageViewBack:
                finish();
                ViewAnimUtils.activityExitTransitions(mActivity);
                break;
        }
    }

    /**
     *get about us screen data
     */
    /*public void getAboutUsRequest() {


        if (AppUtils.isNetworkAvailable(mActivity)) {

            try {

                *//**
                 * show if progress dialog is not showing
                 *//*
                mDialog.show();

                apiService = (APIService) APIService.createService(GeorgewayApi.class);

                AppUtils mApiInterface = ApiClient.getClient().getRetrofitInstance().create(ApiInterface.class);
                Call<ResponseBody> call = mApiInterface.getTERMRequest();

                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        try {
                            *//**
                             * dismiss dialog if open
                             *//*
                            mDialog.hide();

                            ResponseBody mResponseBody = response.body();
                            String about_us =new String(mResponseBody.bytes());


                            if (!TextUtils.isEmpty(about_us)) {

                                *//**
                                 * show about us content
                                 *//*
                                updateDataInWebView(about_us);

                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                        *//**
                         * dismiss dialog if open
                         *//*
                        mDialog.hide();
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            mDialog.show();
        }
    }*/

//    DialogInterface.OnClickListener networkCallBack3 = new DialogInterface.OnClickListener() {
//        @Override
//        public void onClick(DialogInterface dialog, int which) {
//           getAboutUsRequest();
//        }
//    };


    /**
     * update data into web view
     */
    private void updateDataInWebView( String dataHtml){

        String output = dataHtml.replaceAll("\"\"", "\'");
        output = output.replaceAll("&lt;","<");
        output = output.replaceAll("&gt;",">");
        output = output.replaceAll("&amp;nbsp;","<br/>");

        try {
            webViewAboutUs.loadData((URLEncoder.encode(output, "utf-8").replaceAll("\\+", " ")), "text/html; charset=UTF-8", null);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }

    // SSL Error Tolerant Web View Client
    private class SSLTolerentWebViewClient extends WebViewClient {
//
//        @Override
//        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
//            handler.proceed(); // Ignore SSL certificate errors
//        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            /**
             * dismiss dialog if open
             */
            mDialog.show();

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            /**
             * dismiss dialog if open
             */
            mDialog.hide();
        }
    }


}